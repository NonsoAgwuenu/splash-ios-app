//
//  CustomOverlayView.swift
//  splash
//
//  Created by Christopher Kendrick on 6/8/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit


protocol SplashCameraDelegate :class{
    func didCancel(_ overlayView:CustomOverlayView)
    func didShoot(_ overlayView:CustomOverlayView)
    func didToggleFlash(_ overlayView:CustomOverlayView)
    func goToPreview(_ overlayView:CustomOverlayView)
    func didEnd(_ recordedVideos:[NSURL])
    func onMerge(_ mediaUrl:NSURL)
    func didToggleCamera()
    func deleteClip()
}

class CustomOverlayView: UIView {
    
    weak var delegate:SplashCameraDelegate! = nil
    
    @IBOutlet weak var trackView: UIView!
    @IBOutlet weak var shootBtn: UIView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var nextBtnLoader: UIActivityIndicatorView!

    @IBOutlet weak var deleteClip: UIButton!
    
    @IBOutlet weak var fullTrack: UIView!
    let durationViews = [UIView]()
    
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
    }
    
    @IBAction func deleteClip(_ sender: UIButton) {
        self.delegate.deleteClip()
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!

    }
    @IBAction func cancel(_ sender: UITapGestureRecognizer) {
        if self.delegate != nil {
            self.delegate.didCancel(self)
        }
    }
    
    @IBAction func toggleCameraVIew(_ sender: UIButton) {
        self.delegate.didToggleCamera()
    }
    
    @IBAction func toggleFlash(_ sender: UIButton) {
        if self.delegate != nil {
            self.delegate.didToggleFlash(self)
        }
        
    }
    
    @IBAction func shoot(_ sender: UITapGestureRecognizer) {
        if self.delegate != nil {
            self.delegate.didShoot(self)
        }
    }

    @IBAction func done(_ sender: UIButton) {
        self.nextBtnLoader.startAnimating()
        self.nextBtnLoader.isHidden = false
        self.nextBtn.isEnabled = false
        self.nextBtn.setTitle("", for: .normal)
        if self.delegate != nil{
            self.delegate.goToPreview(self)
        }
    }

    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
