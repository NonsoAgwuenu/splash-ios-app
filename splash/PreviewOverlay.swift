//
//  PreviewOverlay.swift
//  splash
//
//  Created by Christopher Kendrick on 6/10/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit

protocol VideoPreviewDelegate:class {
    func videoPreviewOnCancel(_ previewOverlay:PreviewOverlay)
    func videoPreviewOnPost(_ previewOverlay:PreviewOverlay, caption:String)
    func didFinishUploading(_ post:PreviewComp)
}


class PreviewOverlay: UIView, UITextFieldDelegate {
    
    weak var delegate:VideoPreviewDelegate! = nil
    
    @IBOutlet weak var btnLoander: UIActivityIndicatorView!
    @IBOutlet weak var captionTextFieldInput: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var captionTextFieldBottomContraint: NSLayoutConstraint!
    @IBOutlet weak var submitBtnBottomConstraint: NSLayoutConstraint!
    
    @IBAction func cancel(_ sender: AnyObject) {
        print("cancel from view")
        self.delegate.videoPreviewOnCancel(self)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    @IBAction func postVideo(_ sender: UIButton) {
        self.endEditing(true)
        captionTextFieldBottomContraint.constant = 8
        submitBtnBottomConstraint.constant = 8
        self.delegate.videoPreviewOnPost(self, caption: captionTextFieldInput.text!)
    }
    @IBAction func editingDidBegin(_ sender: AnyObject) {
        captionTextFieldInput.delegate = self
        captionTextFieldInput.becomeFirstResponder()
        captionTextFieldBottomContraint.constant = 280
        submitBtnBottomConstraint.constant = 280
        self.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        captionTextFieldInput.resignFirstResponder()
        captionTextFieldBottomContraint.constant = 8
        submitBtnBottomConstraint.constant = 8
        return true
    }
    
}
