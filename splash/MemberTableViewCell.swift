//
//  MemberTableViewCell.swift
//  splash
//
//  Created by Christopher Kendrick on 7/21/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit

class MemberTableViewCell: UITableViewCell {


    @IBOutlet weak var memberUsername: UILabel!
    @IBOutlet weak var btnAction: UIView!
    @IBOutlet weak var profilePic: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
