//
//  CameraViewController.swift
//  splash
//
//  Created by Christopher Kendrick on 9/26/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit
import MediaPlayer
import MobileCoreServices
import AssetsLibrary

protocol CameraUploadDelegate {
    func Camera(post:PreviewComp)
}

class SplashCamera: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate, MediaMergerDelegate, SplashCameraDelegate, VideoPreviewDelegate {
    
    var imagePicker:UIImagePickerController = UIImagePickerController()
    var vc:UIViewController!
    
    var cameraUploadDelegate:CameraUploadDelegate? = nil
    var maxRecordingTime:Int = 30
    var currentDuration:Int = 0
    var cameraOverlayView:CustomOverlayView = CustomOverlayView()
    var delegate:SplashCameraDelegate? = nil
    var isCapturingVideo:Bool! = false
    var mediaMerger:MediaMerger? = MediaMerger()
    var isFlashOn:Bool! = false
    var mergedVideoUrl:URL? = nil
    var customViewController:CameraOverlayViewController!
    var isSplashing:Bool = false
    var splashingPostId:String! = ""
    var didStartShooting:Bool = false
    private var videoRecordingTimer:Timer?
    var playFromToggle:Bool = false
    
    private var mediaBeingMerged:[NSURL] = []
    
    var addedUITrackViews = [UIView]()

    var cloneTrackView:UIView? = nil
    
    var trackViewToAnimate:UIView? = nil
    
    var newItemXPos:CGFloat? = 0
    
    var videoPreview:VideoPreview! = VideoPreview()
    var videoPreViewUrl:URL? = nil
    var isSplashingId:String? = ""
    
    init(vc:UIViewController? = nil) {
        super.init()
        self.delegate = self
        self.vc = vc
        mediaMerger?.delegate = self
        videoPreview.delegate = self
    }
    
    
    
    
    
    
    func videoPreviewOnCancel(_ previewOverlay:PreviewOverlay){
        self.videoPreview.playerViewController.player?.pause()
        self.imagePicker.popViewController(animated: true)
        
    }
    
//    func popViewController(target:UIImagePickerController, animated: Bool, completion:@escaping ()->Void) {
//        CATransaction.begin()
//        CATransaction.setCompletionBlock(completion)
//        target.popViewController(animated: true)
//        CATransaction.commit()
//    }
    
    func videoPreviewOnPost(_ previewOverlay:PreviewOverlay, caption:String){
        let session = self.vc.getSession()
        let userId = session["userId"].stringValue
        
        let videoUrl = self.vc.baseUrl() + "Videos" + "/" + userId + "/upload"
    
        self.videoPreview.uploadPreview(endPoint: videoUrl, caption: caption, file: self.videoPreViewUrl!, isSplashing:self.isSplashing, postId: self.isSplashingId)
    }
    
    func didFinishUploading(_ post:PreviewComp){
        self.cameraUploadDelegate?.Camera(post: post)
        self.vc.dismiss(animated: true, completion: {
            
            
        })
    }
    
    
    
    
    
    
    
    
    
    
    func removeIndicator(){
        
        print(self.cameraOverlayView.fullTrack.subviews.map({ $0 }))
        
        let v1:UIView = self.cameraOverlayView.fullTrack.viewWithTag(self.cameraOverlayView.fullTrack.subviews.count - 1)!
//        let v2:UIView = self.cameraOverlayView.fullTrack.viewWithTag(self.cameraOverlayView.fullTrack.subviews.count)!
        
        //v2.removeFromSuperview()
        v1.removeFromSuperview()
    }
    
    
    // Conform start
    func deleteClip() {
        
        self.removeIndicator()
    }
    
    func didEnd(_ recordedVideos: [NSURL]) {
        
    }
    
    func didCancel(_ overlayView: CustomOverlayView) {
        self.closeCamera()
    }
    
    func didShoot(_ overlayView:CustomOverlayView) {
        self.toggleRecording()
    }
    
    func didToggleFlash(_ overlayView: CustomOverlayView) {
        self.toggleFlash()
    }
    
    func goToPreview(_ overlayView: CustomOverlayView) {
        self.endVideo()
    }
    
    func onMerge(_ mediaUrl:NSURL) {
        
    }
    func didToggleCamera() {
        self.toggleCameraView()
    }
    //

    
    func endVideo() {
        
        if self.isCapturingVideo == false {
            self.stopRecording()
            self.onEnd()
        }else{
            self.currentDuration = 30
        }
        
    }
    
    
    func onEnd() {
        self.currentDuration = 0
        self.delegate?.didEnd(self.mediaBeingMerged)
        self.mediaMerger?.mergeVideoClips(self.mediaBeingMerged as [URL])
    }
    
    func updateDuration()->Int {
        self.currentDuration = self.currentDuration + 1
        if self.currentDuration > 0 {
            self.cameraOverlayView.nextBtn.isHidden = false
        }
        return self.currentDuration
    }
    
    func didVideoEnd()->Bool{
        if self.currentDuration >= self.maxRecordingTime {
            self.videoRecordingTimer?.invalidate()
            return true
        }
        return false
    }
    
    private func startTimer() {
        self.videoRecordingTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateRecordingTimeSeconds), userInfo: nil, repeats: true)
    }
    
    
    func updateRecordingTimeSeconds(){
        if(self.didVideoEnd()){
            self.imagePicker.stopVideoCapture()
        }
        _ = self.updateDuration()
        self.updateClipWidth()
    }
    
    func updateClipWidth(){
        let w = (self.trackViewToAnimate?.layer.bounds.width)! + (self.cameraOverlayView.layer.bounds.width / 30)
        self.trackViewToAnimate?.layer.frame = CGRect(x: self.newItemXPos!, y: 0, width: w, height: 40)
    }


    
    
    func onMergeComplete(mediaUrl: NSURL) {
        self.videoPreViewUrl = mediaUrl as URL
        
        // add video vc delage
        self.videoPreview.previewUrl(url: mediaUrl as URL, imagepicker: self.imagePicker, target:self.vc)
        
        self.mergedVideoUrl = mediaUrl as URL
        self.cameraOverlayView.nextBtnLoader.isHidden = true
        self.cameraOverlayView.nextBtn.setTitle("Post", for: .normal)
        self.cameraOverlayView.nextBtn.isEnabled = true
    }
    
    func openCamera(target:UIViewController) {
        
        self.vc = target
        self.delegate = self
        self.imagePicker.sourceType = .camera
        self.imagePicker.mediaTypes = [kUTTypeMovie as NSString as String]
        self.imagePicker.allowsEditing = false
        self.imagePicker.delegate = self
        self.imagePicker.showsCameraControls = false
        self.imagePicker.cameraFlashMode = UIImagePickerControllerCameraFlashMode.on
        self.imagePicker.videoQuality = UIImagePickerControllerQualityType.typeHigh
        self.imagePicker.cameraDevice = .front
        
        self.customViewController = CameraOverlayViewController(
            nibName:"CameraOverlayViewController",
            bundle: nil)
        
        if self.isSplashing == true {
            self.isSplashing = true
            self.isSplashingId = self.splashingPostId
        }

        
        self.customViewController.cameraVC = self.imagePicker
        
        self.cameraOverlayView = (customViewController.view as? CustomOverlayView)!
        
        self.cameraOverlayView.shootBtn.layer.cornerRadius = self.cameraOverlayView.shootBtn.layer.bounds.width / 2
        self.cameraOverlayView.shootBtn.layer.backgroundColor = UIColor.clear.cgColor
        self.cameraOverlayView.nextBtn.isHidden = true
        self.cameraOverlayView.nextBtnLoader.isHidden = true
        
        self.cameraOverlayView.shootBtn.layer.borderWidth = 5
        self.cameraOverlayView.shootBtn.layer.borderColor = UIColor.white.cgColor
        
        self.cameraOverlayView.delegate = self.delegate
        self.cameraOverlayView.frame = self.imagePicker.view.frame
        self.cameraOverlayView.frame.origin.y = 0
        
        self.cameraOverlayView.nextBtn.layer.backgroundColor = UIColor.clear.cgColor
        self.cameraOverlayView.nextBtn.layer.borderColor = UIColor.white.cgColor
        self.cameraOverlayView.nextBtn.layer.borderWidth = 2
        self.cameraOverlayView.nextBtn.layer.cornerRadius = 4
        
       _ = CGRect(x: 0, y: 0, width: 1, height: 40)
        let indicator = self.getNewIindicator()
        indicator.tag = 0
        self.cameraOverlayView.fullTrack.addSubview(indicator)
        self.trackViewToAnimate = indicator
        
        
        
        
        target.present(self.imagePicker, animated: true, completion: {
            self.mediaBeingMerged = []
            self.currentDuration = 1
            self.imagePicker.cameraOverlayView = self.cameraOverlayView
        })        
    }
    
    
    func getNewIindicator()->UIView{
        let indicator:UIView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 40))
        indicator.backgroundColor = .red
        return indicator
    }
    
    func animateIndicator(view:UIView){
        
    }
    
    
    func cloneBar(view:UIView) {
        
    }
    
    func startFlash() {
        print("start flash")
        self.imagePicker.cameraFlashMode = UIImagePickerControllerCameraFlashMode.on
        self.isFlashOn = true
    }
    
    func stopFlash() {
        print("stop flash")
        self.imagePicker.cameraFlashMode = UIImagePickerControllerCameraFlashMode.off
        self.isFlashOn = false
    }
    
    func startRecording() {
        self.didStartShooting = true
        self.startTimer()
        self.imagePicker.startVideoCapture()
        self.isCapturingVideo = true
        self.cameraOverlayView.shootBtn.layer.opacity = 0.4
        
        if self.addedUITrackViews.count > 0 {
            
        }
    }
    
    func stopRecording() {
        self.videoRecordingTimer?.invalidate()
        self.imagePicker.stopVideoCapture()
        self.isCapturingVideo = false
        self.cameraOverlayView.shootBtn.layer.opacity = 1
        
        // Add to array
        self.addedUITrackViews.append(self.trackViewToAnimate!)
        self.trackViewToAnimate = nil
        
        // Get last item position
        let xPositionOfNewItem = self.addedUITrackViews.reduce(0){
            $0 + $1.layer.bounds.width
        }
        
        self.newItemXPos = xPositionOfNewItem
        
         // Add the view to the list of tracks
        let newIdicatorPosition = CGRect(x: xPositionOfNewItem, y: 0, width: 0, height: 40)
        let indicator = self.getNewIindicator()
        
        let indicatorSeperator:UIView = UIView(frame: CGRect(x: 0, y: 0, width: 1, height: 40))
        indicatorSeperator.backgroundColor = UIColor.white
        indicator.addSubview(indicatorSeperator)
        
        
        //indicator.backgroundColor = self.getRandomColor()
        indicator.tag = self.addedUITrackViews.count
        
        
        self.cameraOverlayView.fullTrack.addSubview(indicator)
        
        let nv = self.cameraOverlayView.viewWithTag(self.addedUITrackViews.count)
        nv?.frame = newIdicatorPosition
        self.trackViewToAnimate = nv
    }
    
    func getRandomColor() -> UIColor{
        //Generate between 0 to 1
        let red:CGFloat = CGFloat(drand48())
        let green:CGFloat = CGFloat(drand48())
        let blue:CGFloat = CGFloat(drand48())
        
        return UIColor(red:red, green: green, blue: blue, alpha: 1.0)
    }
    
    
    func toggleRecording() {
        if self.isCapturingVideo == true {
            self.stopRecording()
            self.playFromToggle = false
        }else {
            self.startRecording()
            self.playFromToggle = true
        }
    }
    
    func toggleFlash() {
        self.toggleTorch()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        let videoURL = info["UIImagePickerControllerMediaURL"] as! NSURL
        _ = self.addMediaToMerge(url: videoURL)
        
        if(self.didVideoEnd()){
            self.onEnd()
        }
    }
    
    func addMediaToMerge(url:NSURL) -> NSURL{
        self.mediaBeingMerged.append(url)
        return url
    }
    
    func removeMediaFromMerge(url:NSURL) {
        let indexOfMedia = self.mediaBeingMerged.index(of: url)
        if indexOfMedia != nil && self.mediaBeingMerged.count > 0 {
            self.mediaBeingMerged.remove(at: indexOfMedia!)
        }
    }
    
    deinit{
        
    }
    
    func closeCamera() {
        
        self.videoRecordingTimer?.invalidate()
        self.imagePicker.dismiss(animated: true, completion: {})
    }
    
    func toggleCameraView() {
        
        if self.isCapturingVideo == true && self.playFromToggle == true {
            self.stopRecording()
        }
        
        if self.imagePicker.cameraDevice == .front {
            self.imagePicker.cameraDevice = .rear
        }else if self.imagePicker.cameraDevice == .rear{
            self.imagePicker.cameraDevice = .front
        }
        
        if self.didStartShooting == true && self.playFromToggle == true {
            self.startRecording()
        }
        
    }
    
    func toggleTorch() {
        let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        if (device?.hasTorch)! {
            do {
                try device?.lockForConfiguration()
                if (device?.torchMode == AVCaptureTorchMode.on) {
                    device?.torchMode = AVCaptureTorchMode.off
                } else {
                    do {
                        try device?.setTorchModeOnWithLevel(1.0)
                    } catch {
                        print(error)
                    }
                }
                device?.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    } 

}
