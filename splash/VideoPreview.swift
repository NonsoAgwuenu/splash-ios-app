//
//  VideoPreview.swift
//  splash
//
//  Created by Christopher Kendrick on 10/1/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import Foundation
import AVKit
import AVFoundation
import Alamofire
import SwiftyJSON


class VideoPreview: NSObject, UINavigationControllerDelegate {
    
    var delegate:VideoPreviewDelegate? = nil
    var previewOverlay:PreviewOverlay?
    var imagepicker:UIImagePickerController? = nil
    let playerViewController = AVPlayerViewController()
    var player:AVPlayer? = nil
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool){
        print("foooooooo")
    }
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool){
        print("BARRRRRRRR")
    }
    
    func previewUrl(url:URL, imagepicker:UIImagePickerController, target:UIViewController) {
        
        
        
        target.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.imagepicker = imagepicker
        self.imagepicker?.navigationController?.delegate = self
        
        self.player = AVPlayer(url: url)
        
        playerViewController.player = self.player
        playerViewController.showsPlaybackControls = false
        playerViewController.navigationController?.delegate = self
        
        playerViewController.modalTransitionStyle = .crossDissolve
        
        let previewOverlay = PreviewOverlayViewController(nibName:
            "PreviewOverlayViewController",
             bundle: nil
        )
        
        
        self.previewOverlay = previewOverlay.view as! PreviewOverlay
        self.previewOverlay?.delegate = self.delegate
        self.previewOverlay?.frame = imagepicker.view.bounds
        
        self.previewOverlay?.btnLoander.isHidden = true
        
        let inputBgColor = UIColor(colorLiteralRed: 255, green: 255, blue: 255, alpha: 0.5)
        self.previewOverlay?.captionTextFieldInput.backgroundColor = inputBgColor
        
        self.previewOverlay?.submitBtn.backgroundColor = UIColor.clear
        self.previewOverlay?.submitBtn.layer.borderColor = UIColor.white.cgColor
        self.previewOverlay?.submitBtn.layer.borderWidth = 2
        
        self.previewOverlay?.submitBtn.layer.cornerRadius = 4
        
        imagepicker.interactivePopGestureRecognizer?.isEnabled = true
        self.playerViewController.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        CATransaction.begin()
        
        self.pushViewController(target: imagepicker, viewController: playerViewController, animated: true, completion: {
            self.playerViewController.view.addSubview(self.previewOverlay!)
            self.playerViewController.player!.play()
        })

    }
    
    func pushViewController(target:UIImagePickerController, viewController: UIViewController, animated: Bool, completion:@escaping ()->Void) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        target.pushViewController(viewController, animated: true)
        CATransaction.commit()
    }
    
    
    func close(){
        //self.playerViewController.player?.pause()
        //self.imagepicker?.popToRootViewController(animated: true)
    }
    
    func handleSwipes(sender:UISwipeGestureRecognizer) {
        if (sender.direction == .left) {
            print("SWIPED LEFT")
        }
    }
    
    
    fileprivate func thumbnailForVideoAtURL(_ url: URL, frame:Int) -> UIImage? {
        
        let asset = AVAsset(url: url)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        
        var time = asset.duration
        time.value = min(time.value, CMTimeValue(frame))
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print("error")
            return nil
        }
    }
    
    func uploadPreview(endPoint:String, image:URL, uploadPhoto:Bool, success:@escaping ()->Void ) {
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if uploadPhoto == true {
                    multipartFormData.append(image, withName: "profileThumbnail")
                }
        },
            to:endPoint,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        success()
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        }
            
        )
    }
    
    
    
    func uploadPreview(endPoint:String, caption:String, file:URL, isSplashing:Bool? = false, postId:String? = "") {
        
        func createThumbnails( completion:( _ img:[UIImage] )->Void ) ->[UIImage]{
            var thumbails = [UIImage]()
            let framesForThumbnails = [1,2,3] as [Int]
            for frameValue in framesForThumbnails {
                
                let thumbnail = self.thumbnailForVideoAtURL(file, frame:frameValue)
                thumbails.append(thumbnail!)
            }
            completion(thumbails)
            return thumbails
        }

        createThumbnails(){ images in

            var thumbnailURLS = [URL]()
            
            //let imageName         = UUID().uuidString + ".jpg"
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
            let photoURL          = NSURL(fileURLWithPath: documentDirectory)
            
            var imageName:String = ""
            var uudId:String = UUID().uuidString
            
            images.enumerated().map({ index, element in
                
                var indexString:String = String(describing: index)
                imageName =  uudId + "-thumbnail-" + indexString + ".jpg"
                let localPath = photoURL.appendingPathComponent(imageName)
                
            
                do {
                    try UIImageJPEGRepresentation(images[1], 1.0)?.write(to: localPath!)
                    thumbnailURLS.append(localPath!)
                }catch {
                    print("error saving file")
                }
                
            })
            
            self.previewOverlay?.btnLoander.startAnimating()
            self.previewOverlay?.btnLoander.isHidden = false
            self.previewOverlay?.submitBtn.setTitle("", for: .normal)
            self.previewOverlay?.submitBtn.isEnabled = false
            
            var vc = UIViewController()
            var session = vc.getSession()
            var userId = session["userId"].stringValue
            

            Alamofire.upload(
                //to:apiUrl,
                //method:.post,
                multipartFormData: { multipartFormData in
                
                    multipartFormData.append(caption.data(using: String.Encoding.utf8)!, withName: "title")
                    multipartFormData.append(userId.data(using: String.Encoding.utf8)!, withName: "memberId")
                    multipartFormData.append(uudId.data(using: String.Encoding.utf8)!, withName: "imageID")
                    multipartFormData.append(file, withName: "video")
                    
                    thumbnailURLS.enumerated().map({ i, e in
                        let formFieldName:String? = "thumbnail-"+String(i)
                        multipartFormData.append(e, withName: formFieldName!)
                    })
                    
                    if isSplashing! == true {
                        multipartFormData.append("true".data(using: String.Encoding.utf8)!, withName: "isSplashing")
                        multipartFormData.append((postId?.data(using: String.Encoding.utf8)!)!, withName: "postId")
                    }
            },
                to:endPoint,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
            
                            //var post = JSON(response.result)
                            var res = JSON(response.result.value)["result"]["post"]
                            var post = PreviewComp()
                            post = post?.instanceFromJSON(video: res)
                            self.delegate?.didFinishUploading(post!)
                            
                            
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                    }
            }
                
            )
            
            
            
            
            
            
            
            
            

        }

    }
    
}
