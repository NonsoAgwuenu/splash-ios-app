//
//  Platform.swift
//  splash
//
//  Created by Christopher Kendrick on 7/14/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

struct Platform {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
            isSim = true
        #endif
        return isSim
    }()
}

