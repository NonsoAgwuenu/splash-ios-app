//
//  CommentViewController.swift
//  splash
//
//  Created by Christopher Kendrick on 12/19/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class Comment {
    
    var id:String!
    var memberId:String!
    var body:String!
    var member:Member!
    init(id:String = "", memberId:String = "", body:String = "", member:Member = Member()){
        self.id = id
        self.memberId = memberId
        self.body = body
        self.member = member
    }
    
    func instanceFromJSON(comment:JSON, me:Member? = nil)->Comment{
        let id = comment["id"].stringValue
        let memberId = comment["memberId"].stringValue
        let body = comment["body"].stringValue
        let memberJson = comment["member"]
        var member:Member? = nil
        
        if me != nil {
            member = Member(id: (me?.id)!, username:(me?.username)!)
        }else{
    
            let postMem:Member = Member()
            member = postMem.instanceFromJSON(member: memberJson)
        }
        
        let comment:Comment = Comment(id: id, memberId: memberId, body: body, member:member!)
        return comment
    }
    
    func instanceArrFromJSONArr(comments:[JSON])->[Comment]{
        return comments.map({ self.instanceFromJSON(comment: $0) })
    }
}

class CommentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var commentTabel: UITableView!
    @IBOutlet weak var inputBox: UITextField!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    
    var video:PreviewComp? = nil
    var comments = [Comment]()
    
    @IBAction func moveup(_ sender: Any) {
        self.bottomConstraint.constant = 176
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getComments(){ comments in
            let comm:Comment = Comment()
            self.comments.append(contentsOf: comm.instanceArrFromJSONArr(comments: comments.arrayValue))
            self.commentTabel.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        guard self.video != nil else {
            return
        }
        
        self.bottomConstraint.constant = 0
        
        self.commentTabel.delegate = self
        self.commentTabel.dataSource = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
        
        
    }

    @IBAction func addComment(_ sender: UIButton) {
        guard (self.inputBox.text?.characters.count)! > 0 else {
            return
        }
        self.createComment(body: self.inputBox.text!)
    }
    
    func createComment(body:String){
        let c:Comment = Comment()
        
        
        self.addComment(comment: body){ newcomment in
            
            let member = self.getMember()
            let memberInst = Member().instanceFromJSON(member: member)
            
            let comment = c.instanceFromJSON(comment: newcomment, me: memberInst)
            self.comments.append(comment)
            self.commentTabel.reloadData()
        }
    }

    
    override func dismissKeyboard() {
        view.endEditing(true)
        self.bottomConstraint.constant = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getComments( completion:@escaping (_ res:JSON)->Void ){
        
        let params = [
            "filter":[
                "include":"member"
            ]
        ]
        
        Alamofire.request(self.baseUrl() + "/Posts/" + (self.video?.id)! + "/comments", method:.get, parameters:params)
            .response(
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    
                    // Validate your JSON response and convert into model objects if necessary
                    
                    if let res = response.result.value {
                        let json = JSON(res)
                        let error = json["error"]
                        
                        // ..2.. If any errors, handle the error
                        if error != nil {
                            _ = self.httpErrorHandler(err: res as AnyObject)
                            return
                        }
                        print(json)
                        completion(json)
                    }
                    
            })
    }
    
    func deleteComment(commentId:String, _ completion:@escaping (_ res:JSON)->Void ){

        Alamofire.request(self.baseUrl() + "/Posts/" + (self.video?.id)! + "/comments/" + commentId, method:.delete)
            .response(
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    
                    // Validate your JSON response and convert into model objects if necessary
                    
                    if let res = response.result.value {
                        let json = JSON(res)
                        let error = json["error"]
                        
                        // ..2.. If any errors, handle the error
                        if error != nil {
                            _ = self.httpErrorHandler(err: res as AnyObject)
                            return
                        }
                        print(json)
                        completion(json)
                        
                        
                    }
                    
            })
        
    }
    
    
    func addComment(comment:String, _ completion:@escaping (_ res:JSON)->Void ){
        print("Getting Post....")
        let sess =  self.getSession()
        let memberId = sess["userId"].stringValue
        
        let params = [
            "body":comment,
            "memberId":memberId
        ] as [String : Any]
        
        Alamofire.request(self.baseUrl() + "/Posts/" + (self.video?.id)! + "/comments", method:.post, parameters: params)
            .response(
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    
                    // Validate your JSON response and convert into model objects if necessary
                    
                    if let res = response.result.value {
                        let json = JSON(res)
                        let error = json["error"]
                        
                        // ..2.. If any errors, handle the error
                        if error != nil {
                            _ = self.httpErrorHandler(err: res as AnyObject)
                            return
                        }
                        print(json)
                        completion(json)
                        
                        
                    }
                    
            })
    
    }
    
    // Override to support editing the table view.
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            self.deleteComment(commentId: self.comments[indexPath.row].id){ count in
                print(count)
            }
            self.comments.remove(at: indexPath.row)
            self.commentTabel.deleteRows(at: [indexPath], with: .fade)
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "commentCell") as! CommentTableViewCell
        var comment = self.comments[indexPath.row]
        
        cell.thumbnail.hnk_setImage(from: URL(string: self.baseUrl(isApi:false) + "videos/" + comment.member.id + "/profile.jpg" ))
        cell.username.text = comment.member.username
        cell.comment.text = comment.body
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comments.count
    }
    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
