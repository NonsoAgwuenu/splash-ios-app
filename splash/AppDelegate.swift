//
//  AppDelegate.swift
//  splash
//
//  Created by Christopher Kendrick on 6/8/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//


import UIKit
import UserNotifications
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
       
        self.registerForPushNotifications(application: application)
        
        func printFonts() {
            for familyName in UIFont.familyNames {
                print("\n-- \(familyName) \n")
                for fontName in UIFont.fontNames(forFamilyName: familyName) {
                    print(fontName)
                }
            }
        }
        //printFonts()
        
        
        let sessionStore = UserDefaults.standard
        let s = sessionStore.object(forKey: "session")
        
        if s != nil {
            
            
            // Since the user have a access token we can now take them to the appropriate page
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //let tabNavController:UINavigationController = storyboard.N
            let previewViewController = storyboard.instantiateViewController(withIdentifier: "mainTabs")
            self.window?.rootViewController = previewViewController
            self.window?.makeKeyAndVisible()
            
            
        }else {
            // If the user dont have a access token
            self.logout()
        }
        
        return true
    }
    
    
    func logout(){
        let storyboard = UIStoryboard(name: "Authentication", bundle: nil)
        let previewViewController = storyboard.instantiateViewController(withIdentifier: "login")
        self.window?.rootViewController = previewViewController
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // Push notifications code start here
    
    func registerForPushNotifications(application: UIApplication) {
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            // Enable or disable features based on authorization.
        }
        application.registerForRemoteNotifications()
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = String(deviceToken.hexString())
        
        if (deviceTokenString != nil) {
            let sessionStore = UserDefaults.standard
            sessionStore.set(deviceTokenString!, forKey: "deviceToken")
            print("Device Token:", deviceTokenString!)
        }
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register:", error)
    }
    
    
    func application(_ application: UIApplication,  didReceiveRemoteNotification userInfo: [NSObject : AnyObject],  fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        var alertInfo = JSON(userInfo)
        print(alertInfo)
    }

}

extension Data {
    func hexString() -> String {
        return self.reduce("") { string, byte in
            string + String(format: "%02X", byte)
        }
    }
}
