//
//  PostPlayerViewController.swift
//  splash
//
//  Created by Christopher Kendrick on 8/18/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import SwiftyJSON

import MediaPlayer
import MobileCoreServices
import AssetsLibrary
import SwiftyJSON
import SwiftDate
import Alamofire

class PostPlayerViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, PlayerOverDelegate{


    var previewComp:PreviewComp!
    var player:AVQueuePlayer!
    
    var splashes = [PreviewComp]()
    var currentIndexInArray:Int = 1
    var fullVideoQueue = [AVPlayerItem]()
    var totalNumberOfVideos:Int = 0
    var videoPlayerOverlay:PlayerOverlay? = nil
    var timer:Timer? = nil
    
    var videoIndicatorFillers = [UIView]()
    var indicatorDividedWidth:CGFloat = 0
    var startAtSplashId:String?
    var splashAddedLabel:UILabel?
    var acceptBtnStackVIew:UIView?
    var currentSplashId:String?
    var selectedSplash:PreviewComp?
    var selectedMember:Member?
    var hideMenu:Bool = false
    var currentComp:PreviewComp?
    
    func updatePostStatus(){
    
    }
    
    func onDelete(){
        self.confirm(title: "Delete Video", message: "Are you sure you want to delete this post and all of it's splashes?", cb:
        {
            self.deletePost(){ count in
                self.alert(message: "Deleted Post Successfully", cb: {
                    self.onTapExit()
                })
            }
        }, onCancel: {
        
        })
    }
    
    func onEdit() {
        if (self.videoPlayerOverlay?.textInputBox.isHidden)! {
            self.videoPlayerOverlay?.textInputBox.isHidden = false
            if(self.currentComp != nil){
                self.videoPlayerOverlay?.textInputBox.text = self.previewComp.caption
            }
            self.videoPlayerOverlay?.isEditingInput = true
            self.videoPlayerOverlay?.editBtn.setTitle("Done", for: .normal)
            
        }else{
            self.videoPlayerOverlay?.textInputBox.isHidden = true
            self.videoPlayerOverlay?.editBtn.setTitle("Edit", for: .normal)
            self.dismissKeyboard()
            self.updatePost(caption: (self.videoPlayerOverlay?.textInputBox.text)!){ res in
                if(res != nil){
                 self.previewComp.caption = res["title"].stringValue
                }
            }
        }
    }
    
    func onDenyPost() {
        
        self.player.pause()
        
        var acceptPostId:String?
        
        if((self.currentSplashId) != nil){
            acceptPostId = self.currentSplashId!
        }
        
        if(self.startAtSplashId != nil) {
            acceptPostId = self.startAtSplashId!
        }
        
        self.updatePostStatus(postId: self.previewComp.id, splashId: acceptPostId!, isAccepted:false){ res in
            let isAccepted = res["accepted"]["isAccepted"].boolValue
            self.videoPlayerOverlay?.loadView.subviews[self.currentIndexInArray - 1].removeFromSuperview()
            self.fullVideoQueue.remove(at: self.currentIndexInArray - 1)
            self.videoIndicatorFillers.remove(at: self.currentIndexInArray - 1)
        }
        
        self.splashAddedLabel?.text = "DENIED POST"
        self.splashAddedLabel?.font = UIFont(name: "Arial", size: 25)
        self.splashAddedLabel?.textColor = .red
        self.splashAddedLabel?.isHidden = false
        
        Timer.scheduledTimer(timeInterval: 0.8, target: BlockOperation(block: {
            print("denied timer")
            self.onTapExit()
        }), selector: #selector(Operation.main), userInfo: nil, repeats: false)
    }
    
    func onAcceptPost() {
        
        self.player.pause()
        
        var acceptPostId:String?
        
        if((self.currentSplashId) != nil){
            acceptPostId = self.currentSplashId!
        }
        
        if(self.startAtSplashId != nil) {
            acceptPostId = self.startAtSplashId!
        }
        
        self.updatePostStatus(postId: self.previewComp.id, splashId: acceptPostId!, isAccepted:true){ res in
            let isAccepted = res["accepted"]["isAccepted"].boolValue
            self.videoPlayerOverlay?.splashAddedLabel.isHidden = false
            
            if(self.startAtSplashId == nil){
                self.videoPlayerOverlay?.addSplashBtn.isHidden = false
                self.videoPlayerOverlay?.splashBtnImg.isHidden = false
                self.videoPlayerOverlay?.acceptBtns.isHidden = true
            }
            
            self.player.play()
        }

        self.splashAddedLabel?.text = "ACCEPTED POST"
        self.splashAddedLabel?.font = UIFont(name: "Arial", size: 25)
        self.splashAddedLabel?.textColor = .green
        self.splashAddedLabel?.isHidden = false
        
        Timer.scheduledTimer(timeInterval: 0.8, target: BlockOperation(block: {
            self.goToNext()
        }), selector: #selector(Operation.main), userInfo: nil, repeats: false)
        
    }
    
    // must be internal or public.
    func update() {
        print("remove label")
        // Something cool
    }
    
    func deletePost( completion:@escaping (_ res:JSON)->Void ){
        let accessToken = self.getSession()["id"].stringValue
        let headers = [
            "Authorization":accessToken
        ]
        
        let url:String = self.baseUrl() + "Members/" + self.getSession()["userId"].stringValue + "/posts/" + self.previewComp.id
        
        Alamofire.request(url, method:.delete, headers:headers)
            .response(
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    
                    print(response)
                    
                    // Validate your JSON response and convert into model objects if necessary
                    
                    if let res = response.result.value {
                        let json = JSON(res)
                        let error = json["error"]
                        
                        // ..2.. If any errors, handle the error
                        if error != nil {
                            _ = self.httpErrorHandler(err: res as AnyObject)
                            return
                        }
                        
                        completion(json)
                        
                    }
                    
            }
        )
    }
    
    
    func updatePost(caption:String, completion:@escaping (_ res:JSON)->Void ){
        let accessToken = self.getSession()["id"].stringValue
        let headers = [
            "Authorization":accessToken
        ]
        
        let params = [
            "title":caption
        ]
        
        let url:String = self.baseUrl() + "Members/" + self.getSession()["userId"].stringValue + "/posts/" + self.previewComp.id
        
        Alamofire.request(url, method:.put, parameters:params, headers:headers)
            .response(
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    
                    print(response)
                    
                    // Validate your JSON response and convert into model objects if necessary
                    
                    if let res = response.result.value {
                        let json = JSON(res)
                        let error = json["error"]
                        
                        // ..2.. If any errors, handle the error
                        if error != nil {
                            _ = self.httpErrorHandler(err: res as AnyObject)
                            return
                        }
                        
                        completion(json)
                        
                    }
                    
            }
        )
    }
    
    func updatePostStatus(postId:String, splashId:String, isAccepted:Bool, _ completion:@escaping (_ res:JSON)->Void ){
        
        
        
        let accessToken = self.getSession()["id"].stringValue
        let headers = [
            "Authorization":accessToken
        ]
        
        let params = [
            "id":self.getSession()["userId"].stringValue,
            "postId":postId,
            "splashId":splashId,
            "isAccepted":isAccepted
        ] as [String : Any]
        
        let url:String = self.baseUrl() + "Members/" + self.getSession()["userId"].stringValue + "/AcceptVideo"
        
        Alamofire.request(url, method:.post, parameters:params, headers:headers)
            .response(
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    
                    // Validate your JSON response and convert into model objects if necessary
                    
                    if let res = response.result.value {
                        let json = JSON(res)
                        let error = json["error"]
                        
                        // ..2.. If any errors, handle the error
                        if error != nil {
                            _ = self.httpErrorHandler(err: res as AnyObject)
                            return
                        }
                        
                        completion(json)
                        
                    }
                    
            }
        )
    
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        let selectedSplash = self.splashes.filter{$0.id == self.startAtSplashId}.first
        guard selectedSplash == nil else {
            self.dismiss(animated: true, completion: {
                
            })
            return
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)

        // 1. Set splashes
        self.setSplashes(self.previewComp.splashes!)
        
        self.selectedSplash = self.splashes.filter{$0.id == self.startAtSplashId}.first
        
        if(self.startAtSplashId != nil && selectedSplash != nil){
            
            self.previewComp.videoUrl = selectedSplash?.videoUrl

            
            if((selectedSplash != nil) && (selectedSplash?.isAccepted)!){
                self.videoPlayerOverlay?.addSplashBtn.isHidden = true
                self.videoPlayerOverlay?.splashBtnImg.isHidden = true
                self.videoPlayerOverlay?.acceptBtns.isHidden = true
            }
            self.splashes = [PreviewComp]()
        }
        
        

        self.setup()
        
        

    }
    
    func setup(){
        self.currentComp = self.previewComp
        let mainVideoPlayer = AVPlayerItem(url:self.previewComp.videoUrl)
        self.fullVideoQueue.append(mainVideoPlayer)
        
        NotificationCenter.default.addObserver(self, selector: #selector(PostPlayerViewController.playerDidFinishPlaying(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: mainVideoPlayer)
        
        // Add splashes to queue
        for splash in self.splashes {
            let playerItem = AVPlayerItem(url:splash.videoUrl)
            NotificationCenter.default.addObserver(self, selector: #selector(PostPlayerViewController.playerDidFinishPlaying(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
            self.fullVideoQueue.append(playerItem)
        }
        
        
        
        self.player = AVQueuePlayer(items: fullVideoQueue)
        self.playVideo(self.fullVideoQueue)
        
        
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(getCurrentPlayerTime), userInfo: nil, repeats: true)
        self.timer?.fire()
    }
    
    func getCurrentPlayerTime(){
        //print(Float(self.player.currentTime().seconds * 100 / (self.player.currentItem?.asset.duration.seconds)! ))
    }
    
    func onAddSplash(){
        self.player.pause()
        
        
//        let splashCamera:SplashCamera! = SplashCamera()
//        splashCamera.openCamera(target:self)
        
        
        var camera:SplashCamera = SplashCamera()
        camera.isSplashing = true
        camera.splashingPostId = self.previewComp.id
        camera.openCamera(target:self)
    }
    
    func onTapExit() {
        
        self.player.pause()
        self.player.removeAllItems()
        self.timer?.invalidate()
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func setSplashes(_ _splashes:[JSON]){
        
        print(_splashes)
        
        for splash in _splashes {
            
            let videoUrlStr = self.baseUrl(isApi: false) + splash["videoUrl"].stringValue
            let urlS = videoUrlStr.addingPercentEscapes(using: String.Encoding.utf8)
            
            // Splash Video
            let videoUrl = URL(string: urlS!, relativeTo:nil)
            let id = splash["id"].stringValue
            let title = splash["title"].stringValue
            let created = splash["createdAt"].stringValue
            let updated = splash["updatedAt"].stringValue
            let isAccepted = splash["isAccepted"].boolValue
            let _memberId = splash["memberId"].stringValue
            
            // Splash member
            let member = splash["member"]
            
            let memberId = member["id"].stringValue
            let lname = member["lastname"].stringValue
            let fname = member["firstname"].stringValue
            let bio = member["bio"].stringValue
            let username = member["username"].stringValue
            let email = member["email"].stringValue
            
            _ = self.getSession()
            let memberInstance = Member(id: memberId, lastname: lname, firstname: fname, bio: bio, username: username, email: email)
            let v = PreviewComp(id: id, caption: title, videoUrl: videoUrl!, splashes: nil, member:memberInstance, createdAt:created, updatedAt:updated, memberId:_memberId, isAccepted:isAccepted)
            
            self.splashes.append(v!)
            
            

        }
    }
    
    
    fileprivate func thumbnailForVideoAtURL(_ url: URL) -> UIImage? {
        
        let asset = AVAsset(url: url)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print("Error Creating Image")
            return nil
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        get {
            return true
        }
    }

    
    func playVideo(_ fullvideoQ:[AVPlayerItem]){
        
        self.player.actionAtItemEnd = AVPlayerActionAtItemEnd.none
        
        let playerController = AVPlayerViewController()
        
        playerController.player = self.player
        playerController.showsPlaybackControls = false
        
        self.addChildViewController(playerController)
        self.view.addSubview(playerController.view)
        playerController.view.frame = self.view.frame
        
        let playOverlayVC:PlayerViewController = PlayerViewController(nibName: "PlayerOverlay", bundle: nil)
        
        self.videoPlayerOverlay = playOverlayVC.view as! PlayerOverlay
        
        print("MY ID", self.getSession()["userId"].stringValue)
        print("VID ID", self.previewComp.memberId!)
        
        // Dont' own the post
        if(self.previewComp.memberId! != self.getSession()["userId"].stringValue){
            print("HIDE EDIT BTN")
            print(self.videoPlayerOverlay?.editBtn)
            self.videoPlayerOverlay?.editBtn.isHidden = true
            self.videoPlayerOverlay?.delBtn.isHidden = true
            
        }
        
        
        self.videoPlayerOverlay?.isUserInteractionEnabled = true
        self.videoPlayerOverlay?.frame = self.view.bounds
        self.videoPlayerOverlay?.delegate = self
        self.videoPlayerOverlay?.loadViewWidthConstraint.constant = self.view.bounds.width
        self.videoPlayerOverlay?.addSplashBtn.layer.borderWidth = 3
        self.videoPlayerOverlay?.addSplashBtn.layer.borderColor = UIColor.white.cgColor
        self.videoPlayerOverlay?.addSplashBtn.layer.cornerRadius = (self.videoPlayerOverlay?.addSplashBtn.layer.bounds.width)! / 2
        self.videoPlayerOverlay?.bottomLabel.text = self.previewComp.caption
        self.videoPlayerOverlay?.subLabel.text = self.previewComp.member?.username
        self.splashAddedLabel = self.videoPlayerOverlay?.splashAddedLabel
        self.videoPlayerOverlay?.thumbnail.layer.cornerRadius = (self.videoPlayerOverlay?.thumbnail.bounds.width)! / 2
        self.videoPlayerOverlay?.thumbnail.clipsToBounds = true
        
        let profilePhotoUrlString = self.baseUrl(isApi: false) + "videos/" + (self.previewComp.member?.id)! + "/profile.jpg"
        self.videoPlayerOverlay?.thumbnail.hnk_setImage(from: URL(string: profilePhotoUrlString))
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PostPlayerViewController.connected(_:)))
        self.videoPlayerOverlay?.thumbnail.addGestureRecognizer(tapGestureRecognizer)
        self.videoPlayerOverlay?.thumbnail.isUserInteractionEnabled = true
        
        self.videoPlayerOverlay?.timestamp.text = self.previewComp.getFriendlyTime()
        
        var session = self.getSession()
        
        
        var leftSwipe = UITapGestureRecognizer(target: self, action: #selector(self.handleSwipes(sender:)))
        self.videoPlayerOverlay?.addGestureRecognizer(leftSwipe)
        
        let downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeDown))
        downSwipe.direction = .down
        self.videoPlayerOverlay?.addGestureRecognizer(downSwipe)
        

        self.videoPlayerOverlay?.acceptBtn.layer.backgroundColor = UIColor.clear.cgColor
        self.videoPlayerOverlay?.acceptBtn.layer.borderWidth = 2
        self.videoPlayerOverlay?.acceptBtn.layer.borderColor = UIColor.green.cgColor
        self.videoPlayerOverlay?.acceptBtn.layer.cornerRadius = 25
        self.videoPlayerOverlay?.acceptBtn.tintColor = UIColor.green
        
        self.videoPlayerOverlay?.denyBtn.layer.backgroundColor = UIColor.clear.cgColor
        self.videoPlayerOverlay?.denyBtn.layer.borderWidth = 2
        self.videoPlayerOverlay?.denyBtn.layer.borderColor = UIColor.red.cgColor
        self.videoPlayerOverlay?.denyBtn.layer.cornerRadius = 25
        self.videoPlayerOverlay?.denyBtn.tintColor = UIColor.red
        
        self.acceptBtnStackVIew = self.videoPlayerOverlay?.acceptBtns
        
        if(self.startAtSplashId != nil  && (self.selectedSplash != nil)){
            
            self.videoPlayerOverlay?.addSplashBtn.isHidden = true
            self.videoPlayerOverlay?.splashBtnImg.isHidden = true
            self.videoPlayerOverlay?.acceptBtns.isHidden = false
            
            if(self.selectedSplash?.isAccepted)!{
                self.videoPlayerOverlay?.acceptBtns.isHidden = true
                
                self.splashAddedLabel?.text = "ACCEPTED POST"
                self.splashAddedLabel?.font = UIFont(name: "Arial", size: 25)
                self.splashAddedLabel?.textColor = .green
                self.splashAddedLabel?.isHidden = false
            }
        }
        
        
        for (index,video) in self.fullVideoQueue.enumerated() {
            
            var width:CGFloat! = 0
            
            if index > self.fullVideoQueue.count {
                width = CGFloat(self.view.bounds.width + 2) / CGFloat(self.fullVideoQueue.count)
            }else{
                width = CGFloat(self.view.bounds.width) / CGFloat(self.fullVideoQueue.count)
            }
            
            self.indicatorDividedWidth = width
            
            let indicatorY = self.view.bounds.height - (self.videoPlayerOverlay?.loadView.bounds.height)!
            let indicatorX = CGFloat(width) * CGFloat(index)
            let indicatorRect = CGRect(x: indicatorX, y: 0, width:width - 2, height: (self.videoPlayerOverlay?.loadView.bounds.height)!)
            let indicator:UIView = UIView(frame: indicatorRect)
            indicator.clipsToBounds = true
            indicator.layer.opacity = 0.4
            indicator.backgroundColor = UIColor.white
            
            

            let indicatorFillRect = CGRect(x:0 - self.indicatorDividedWidth, y: 0, width: self.indicatorDividedWidth, height: (self.videoPlayerOverlay?.loadView.bounds.height)!)
            let indicatorFill = UIView(frame: indicatorFillRect)
            indicatorFill.backgroundColor = UIColor.red
            
            self.videoIndicatorFillers.append(indicatorFill)
            
            indicator.addSubview(indicatorFill)
            
            self.videoPlayerOverlay?.loadView.addSubview(indicator)
        }
        
        self.videoPlayerOverlay?.loadView.subviews.first?.layer.opacity =  1
        
        
        //print(self.splashes[currentIndexInArray].caption)
        
        if self.currentIndexInArray == 0 {
            //self.videoPlayerOverlay?.bottomLabel.text = self.previewComp.caption
        }
        
        
        
        self.view.addSubview(self.videoPlayerOverlay!)
        self.view.bringSubview(toFront: self.videoPlayerOverlay!)
        
        
        
        
        self.player.play()
        

        self.delay(1, closure: {
            UIView.animate(withDuration: self.fullVideoQueue[0].duration.seconds, delay: 0, options: [.curveEaseOut], animations: {
                self.videoPlayerOverlay?.loadView.subviews.first?.subviews[0].frame.origin = CGPoint(x: 0, y: 0)
                //self.view.layoutIfNeeded()
            }, completion: nil)
        })
        
    }
    
    func swipeDown(){
        self.onTapExit()
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    func remoteFileExists(url: String) -> Bool {
        
        var exists: Bool = false
        let url: NSURL = NSURL(string: url)!
        var request: NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "HEAD"
        var response: URLResponse?
        
        do{
            try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response)
        }catch{
            print("error")
        }
        
        if let httpResponse = response as? HTTPURLResponse {
            
            if httpResponse.statusCode == 200 {
                
                exists =  true
            }else{
                exists  = false
            }
            
        }
        
        return exists
    }
    
    
    func goToNext(){
        
        self.splashAddedLabel?.isHidden = true
        
        if self.currentIndexInArray < (self.videoPlayerOverlay?.loadView.subviews.count)! {
            
            self.selectedSplash = splashes[self.currentIndexInArray - 1]
            self.currentComp = splashes[self.currentIndexInArray - 1]
            
            let isAccepted = self.splashes[self.currentIndexInArray - 1].isAccepted
            self.currentSplashId = self.splashes[self.currentIndexInArray - 1].id
            
            
            if(!isAccepted && (self.previewComp.memberId == self.getSession()["userId"].stringValue)){
                self.videoPlayerOverlay?.acceptBtns.isHidden = false
                self.videoPlayerOverlay?.addSplashBtn.isHidden = true
                self.videoPlayerOverlay?.splashBtnImg.isHidden = true
            }
            
//            if(self.selectedSplash!.isAccepted){
//                self.videoPlayerOverlay?.acceptBtns.isHidden = true
//                self.splashAddedLabel?.text = "ACCEPTED POST"
//                self.splashAddedLabel?.font = UIFont(name: "Arial", size: 25)
//                self.splashAddedLabel?.textColor = .green
//                self.splashAddedLabel?.isHidden = false
//            }
            
            self.videoPlayerOverlay?.loadView.subviews[self.currentIndexInArray].layer.opacity = 1
            self.videoPlayerOverlay?.bottomLabel.text = self.splashes[self.currentIndexInArray - 1].caption
            self.videoPlayerOverlay?.subLabel.text = self.splashes[self.currentIndexInArray - 1].member?.username
            
            self.videoPlayerOverlay?.timestamp.text = self.splashes[self.currentIndexInArray - 1].getFriendlyTime()
            
            let profilePhotoUrlString = self.baseUrl(isApi: false) + "videos/" + (self.splashes[self.currentIndexInArray - 1].memberId)! + "/profile.jpg"
            
            
            self.videoPlayerOverlay?.thumbnail.hnk_setImage(from: URL(string: profilePhotoUrlString))
            self.videoPlayerOverlay?.loadView.subviews[self.currentIndexInArray - 1].subviews[0].layer.removeAllAnimations()
            self.videoPlayerOverlay?.loadView.subviews[self.currentIndexInArray - 1].subviews[0].frame.origin = CGPoint(x: 0, y: 0)
            
            self.currentIndexInArray += 1
            //self.videoPlayerOverlay?.bottomLabel.text = self.splashes[currentIndexInArray].caption
            self.player.advanceToNextItem()
            
            
            self.delay(0.1, closure: {
                UIView.animate(withDuration: self.fullVideoQueue[self.currentIndexInArray - 1].duration.seconds, delay: 0, options: [.curveEaseOut], animations: {
                    self.videoPlayerOverlay?.loadView.subviews[self.currentIndexInArray - 1].subviews[0].frame.origin = CGPoint(x: 0, y: 0)
                    self.view.layoutIfNeeded()
                }, completion: nil)
            })
            
        }else{
            
            self.currentIndexInArray = 1
            
            self.currentComp = self.previewComp
            
            self.player.removeAllItems()
            
            for loader in (self.videoPlayerOverlay?.loadView.subviews)! {
                loader.subviews[0].frame.origin = CGPoint(x: 0 - self.indicatorDividedWidth, y: 0)
            }
            
            for player in self.fullVideoQueue{
                player.seek(to: kCMTimeZero)
                self.player.insert(player, after: nil)
            }
            
            self.player.seek(to: kCMTimeZero)
            
            for view in (self.videoPlayerOverlay?.loadView.subviews)! {
                view.layer.opacity = 0.5
            }
            
            self.videoPlayerOverlay?.bottomLabel.text = self.previewComp.caption
            self.videoPlayerOverlay?.subLabel.text = self.previewComp.member?.username
            self.videoPlayerOverlay?.loadView.subviews.first?.layer.opacity =  1
            self.videoPlayerOverlay?.timestamp.text = self.previewComp.getFriendlyTime()
            
            self.videoPlayerOverlay?.addSplashBtn.isHidden = false
            self.videoPlayerOverlay?.splashBtnImg.isHidden = false
            self.videoPlayerOverlay?.acceptBtns.isHidden = true
            
            let profilePhotoUrlString = self.baseUrl(isApi: false) + "videos/" + (self.previewComp.member?.id)! + "/profile.jpg"
            print(profilePhotoUrlString)
            self.videoPlayerOverlay?.thumbnail.hnk_setImage(from: URL(string: profilePhotoUrlString))
            
            if(self.startAtSplashId != nil ){
                
                self.videoPlayerOverlay?.addSplashBtn.isHidden = true
                self.videoPlayerOverlay?.splashBtnImg.isHidden = true
                self.videoPlayerOverlay?.acceptBtns.isHidden = false
                
                if(self.selectedSplash?.isAccepted)!{
                    self.videoPlayerOverlay?.acceptBtns.isHidden = true
                    self.splashAddedLabel?.text = "ACCEPTED POST"
                    self.splashAddedLabel?.font = UIFont(name: "Arial", size: 25)
                    self.splashAddedLabel?.textColor = .green
                    self.splashAddedLabel?.isHidden = false
                }
                
            }
        
//            if(self.selectedSplash?.isAccepted)!{
//                self.videoPlayerOverlay?.acceptBtns.isHidden = true
//                self.splashAddedLabel?.text = "ACCEPTED POST"
//                self.splashAddedLabel?.font = UIFont(name: "Arial", size: 25)
//                self.splashAddedLabel?.textColor = .green
//                self.splashAddedLabel?.isHidden = false
//            }
            
            if self.remoteFileExists(url: profilePhotoUrlString) {
                let photoUrl = NSURL(string:profilePhotoUrlString)
                if photoUrl != nil {
                    let imgData:UIImage = UIImage(data: NSData(contentsOf: photoUrl as! URL)! as Data)!
                    
                    
                    //self.thumbnail.image =  UIImage(data: NSData(contentsOf: photoUrl as! URL)! as Data)
                }
            }
            
            self.delay(0.1, closure: {
                UIView.animate(withDuration: (self.fullVideoQueue.first?.duration.seconds)!, delay: 0, options: [.curveEaseOut], animations: {
                    self.videoPlayerOverlay?.loadView.subviews.first?.subviews[0].frame.origin = CGPoint(x: 0, y: 0)
                    self.view.layoutIfNeeded()
                }, completion: nil)
            })
        }
    }
    
    func goToPrev(){
        if self.currentIndexInArray < (self.videoPlayerOverlay?.loadView.subviews.count)! {
            
            self.currentIndexInArray -= 1
            
            self.videoPlayerOverlay?.loadView.subviews[self.currentIndexInArray].layer.opacity = 1
            self.videoPlayerOverlay?.bottomLabel.text = self.splashes[self.currentIndexInArray].caption
            self.videoPlayerOverlay?.subLabel.text = self.splashes[self.currentIndexInArray].member?.username
            
            self.videoPlayerOverlay?.timestamp.text = self.splashes[self.currentIndexInArray].getFriendlyTime()
            
            self.videoPlayerOverlay?.thumbnail.image = self.splashes[self.currentIndexInArray].thumbnail
            
            self.videoPlayerOverlay?.loadView.subviews[self.currentIndexInArray].subviews[0].layer.removeAllAnimations()
            self.videoPlayerOverlay?.loadView.subviews[self.currentIndexInArray].subviews[0].frame.origin = CGPoint(x: 0, y: 0)
            
            //self.videoPlayerOverlay?.bottomLabel.text = self.splashes[currentIndexInArray].caption
            self.player.advanceToNextItem()
            
            
            self.delay(0.1, closure: {
                UIView.animate(withDuration: self.fullVideoQueue[self.currentIndexInArray - 1].duration.seconds, delay: 0, options: [.curveEaseOut], animations: {
                    self.videoPlayerOverlay?.loadView.subviews[self.currentIndexInArray - 1].subviews[0].frame.origin = CGPoint(x: 0, y: 0)
                    self.view.layoutIfNeeded()
                }, completion: nil)
            })
        }
    }
    
    
    func handleSwipes(sender:UITapGestureRecognizer) {
        
        self.dismissKeyboard()
        
        let point:CGPoint = sender.location(in: self.view)
        let leftPoint = self.view.bounds.width/2
        
        if point.x >= leftPoint {
            self.goToNext()
        }else if point.x <= leftPoint {
            self.goToNext()
        }
        
    }
    
    func isPlayingLastVideo () -> Bool{
        if self.fullVideoQueue.count == self.totalNumberOfVideos {
            return true
        }
        return false
    }
    
    @objc func playerDidFinishPlaying(_ note: Notification) {
        self.goToNext()
    }
    
    func connected(_ sender:AnyObject){
//        if self.currentIndexInArray < (self.videoPlayerOverlay?.loadView.subviews.count)! {
//            self.selectedSplash = splashes[self.currentIndexInArray - 1]
//        }else{
//             self.currentIndexInArray = 1
//            
//             self.selectedSplash = splashes[self.currentIndexInArray - 1]
//        }
        super.performSegue(withIdentifier: "playerToProfile", sender: self)
        
        
    }

    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "playerToProfile"{
            let profileVC:ProfileViewController = segue.destination as! ProfileViewController
            
            if(self.currentIndexInArray == 1){
                profileVC.profileId = self.previewComp.memberId
            }else{
                profileVC.profileId = self.selectedSplash!.memberId
            }
            //profileVC.member = self.selectedMember
        }
    }

    
}
