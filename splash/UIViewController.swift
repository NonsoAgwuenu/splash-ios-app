//
//  UIViewController.swift
//  splash
//
//  Created by Christopher Kendrick on 7/13/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol keyboardDelegate {
    func onExit()
}

extension UIViewController {
    
    func baseUrl(isApi:Bool = true) -> String {
        if isApi == true {
            if Platform.isSimulator {
                return "http://35.164.254.236:4000/api/"
            }else{
                return "http://35.164.254.236:4000/api/"
            }
        }else{
            if Platform.isSimulator {
                return "http://35.164.254.236:4000/"
            }else{
                return "http://35.164.254.236:4000/"
            }
        }
    }
    
    // Http Error Handler
    
    func httpErrorHandler( err:AnyObject )->AnyObject{
        
        let error = JSON(err["error"])
        
        
        if error.exists() {
            let message = error["message"].stringValue
            var code = error["code"].stringValue
            
            if code == "MODEL_NOT_FOUND"{
                //print(code)
                
            }else{
                let alert = UIAlertController(title: "Error", message: message , preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        return error as AnyObject
    }
    
    // Go to login page
    
    func saveMember(member:JSON) {
        let sessionStore = UserDefaults.standard
        sessionStore.set(member.dictionaryObject, forKey: "member")
    }
    
    func getMember()->JSON {
        let sessionStore = UserDefaults.standard
        return JSON(sessionStore.object(forKey: "member")!)
    }
    
    func goToPage(_ storyboardName:String!, viewControllerName:String!) {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let previewViewController = storyboard.instantiateViewController(withIdentifier: viewControllerName)
        self.present(previewViewController, animated: true, completion:nil)
    }
    
    // Save user session
    
    func saveSession(_ session:JSON) {
        print(session)
        let sessionStore = UserDefaults.standard
        sessionStore.set(session.dictionaryObject, forKey: "session")
    }
    
    // Get user session
    
    func getSession() -> JSON{
        let sessionStore = UserDefaults.standard
        return JSON(sessionStore.object(forKey: "session")!)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func onKeyboardExit(){
    
    }
    func confirm(title:String, message:String, btnTitle:String? = "Ok", cb:@escaping ()->Void, onCancel:@escaping ()->Void ){
        
        let refreshAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: btnTitle, style: .default, handler: { (action: UIAlertAction!) in
            cb()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            onCancel()
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
    }
    
    func alert(message: String, title: String = "", cb:@escaping ()->Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            cb()
            })
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
}
