//
//  MediaMerger.swift
//  splash
//
//  Created by Christopher Kendrick on 9/29/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit
import AssetsLibrary
import AVFoundation

protocol MediaMergerDelegate:class {
    func onMergeComplete(mediaUrl:NSURL)
    var mergedVideoUrl:URL?{get set}
}



class MediaMerger {
    
    
    var delegate:MediaMergerDelegate? = nil
    var exportSession:AVAssetExportSession!
    
    func mergeVideoClips(_ videoArr:[URL]){

        let composition = AVMutableComposition()
        
        let videoTrack = composition.addMutableTrack(withMediaType: AVMediaTypeVideo, preferredTrackID: kCMPersistentTrackID_Invalid)
        let audioTrack = composition.addMutableTrack(withMediaType: AVMediaTypeAudio, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        var layerInstructions = [AVVideoCompositionLayerInstruction]()

        let time:CMTime = CMTimeMake(0, 0)
        for video in videoArr {

            let asset = AVAsset(url: video)

            let videoAssetTrack = asset.tracks(withMediaType: AVMediaTypeVideo)[0]
            let audioAssetTrack = asset.tracks(withMediaType: AVMediaTypeAudio)[0]

            let atTime = CMTime(seconds: time.seconds, preferredTimescale:time.timescale)

            do{
                try videoTrack.insertTimeRange(CMTimeRangeMake(kCMTimeZero, asset.duration), of: videoAssetTrack, at: atTime)
                try audioTrack.insertTimeRange(CMTimeRangeMake(kCMTimeZero, asset.duration), of: audioAssetTrack, at: atTime)
            }catch{
                print("something bad happend I don't want to talk about it")
            }

            let instruction = videoCompositionInstructionForTrack(videoTrack, asset: asset)
            layerInstructions.append(instruction)

            //print(self.getProgress(asset.duration.seconds))
            
            CMTimeMakeWithSeconds(CMTimeGetSeconds(asset.duration + time), asset.duration.timescale)
            
        }




         //2.1

        let instructionTimeRange  = Double(30)
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTime(seconds: instructionTimeRange, preferredTimescale: 1))
        mainInstruction.layerInstructions = layerInstructions
        
        let mainComposition = AVMutableVideoComposition()
        mainComposition.instructions = [mainInstruction]
        mainComposition.frameDuration = CMTimeMake(1, 30)
        mainComposition.renderSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
        // 4 - Get path
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]

        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        let date = dateFormatter.string(from: Date())
        
        let uuid = UUID().uuidString

        let savePath = (documentDirectory as NSString).appendingPathComponent("mergeVideo-\(uuid).mov")
        let url = URL(fileURLWithPath: savePath)

        // 5 - Create Exporter
        self.exportSession = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality)
        self.exportSession.outputURL = url
        self.exportSession.outputFileType = AVFileTypeQuickTimeMovie
        self.exportSession.shouldOptimizeForNetworkUse = true
        self.exportSession.videoComposition = mainComposition
        
        // 6 - Perform the Export
        self.exportSession.exportAsynchronously() {
            DispatchQueue.main.async { _ in
                self.finalExportCompletion(self.exportSession, completion: { mergedVideoUrl in
                    self.delegate?.onMergeComplete(mediaUrl: mergedVideoUrl as NSURL)
                })
            }
        }
    }

    func finalExportCompletion(_ session: AVAssetExportSession, completion:@escaping ( (URL)->Void ) ) {
        let library = ALAssetsLibrary()
        if library.videoAtPathIs(compatibleWithSavedPhotosAlbum: session.outputURL) {
            var completionBlock: ALAssetsLibraryWriteVideoCompletionBlock

            completionBlock = { assetUrl, error in
                if error != nil {
                    print("error writing to disk")
                } else {
                    completion(session.outputURL!)
                }
            }
            library.writeVideoAtPath(toSavedPhotosAlbum: session.outputURL, completionBlock: completionBlock)
        }
    }
    
    func videoCompositionInstructionForTrack(_ track: AVCompositionTrack, asset: AVAsset) -> AVMutableVideoCompositionLayerInstruction {
                let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
                let assetTrack = asset.tracks(withMediaType: AVMediaTypeVideo)[0]
        
        
                let transform = assetTrack.preferredTransform
                let assetInfo = orientationFromTransform(transform)
        
        
                var scaleToFitRatio = UIScreen.main.bounds.width / assetTrack.naturalSize.width
                if assetInfo.isPortrait {
                    scaleToFitRatio = UIScreen.main.bounds.width / assetTrack.naturalSize.height
                    let scaleFactor = CGAffineTransform(scaleX: scaleToFitRatio, y: scaleToFitRatio)
                    instruction.setTransform(assetTrack.preferredTransform.concatenating(scaleFactor),
                                             at: kCMTimeZero)
                    
                } else {
                    let scaleFactor = CGAffineTransform(scaleX: scaleToFitRatio, y: scaleToFitRatio)
                    var concat = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.width / 2))
                    if assetInfo.orientation == .down {
                        let fixUpsideDown = CGAffineTransform(rotationAngle: CGFloat(M_PI))
                        let windowBounds = UIScreen.main.bounds
                        let yFix = assetTrack.naturalSize.height + windowBounds.height
                        let centerFix = CGAffineTransform(translationX: assetTrack.naturalSize.width, y: yFix)
                        concat = fixUpsideDown.concatenating(centerFix).concatenating(scaleFactor)
                    }
                    
                    instruction.setTransform(concat, at: kCMTimeZero)
                }
                
                return instruction
            }
    
    
    
        func orientationFromTransform(_ transform: CGAffineTransform) -> (orientation: UIImageOrientation, isPortrait: Bool) {
            var assetOrientation = UIImageOrientation.upMirrored
            var isPortrait = false
            if transform.a == 0 && transform.b == 1.0 && transform.c == -1.0 && transform.d == 0 {
                assetOrientation = .rightMirrored
                isPortrait = true
            } else if transform.a == 0 && transform.b == -1.0 && transform.c == 1.0 && transform.d == 0 {
                assetOrientation = .leftMirrored
                isPortrait = true
            } else if transform.a == 1.0 && transform.b == 0 && transform.c == 0 && transform.d == 1.0 {
                assetOrientation = .upMirrored
            } else if transform.a == -1.0 && transform.b == 0 && transform.c == 0 && transform.d == -1.0 {
                assetOrientation = .downMirrored
            }
            return (assetOrientation, isPortrait)
        }

    
    
    
}
