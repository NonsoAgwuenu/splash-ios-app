//
//  CommentTableViewCell.swift
//  splash
//
//  Created by Christopher Kendrick on 12/19/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var timestamp: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.thumbnail.layer.cornerRadius = self.thumbnail.layer.bounds.width / 2
        self.thumbnail.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
