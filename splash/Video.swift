//
//  Preview.swift
//  splash
//
//  Created by Christopher Kendrick on 6/10/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit


class Video: NSObject {
    let username:String!
    let videoUrl:String!
    var caption:String!
    var thumbnail:UIImage?
    
    init?(videolUrl:String, caption:String, thumbnail:UIImage, username:String) {
        self.videoUrl = videolUrl
        self.caption = caption
        self.thumbnail = thumbnail
        self.username = username
        
        if username.isEmpty || videolUrl.isEmpty || caption.isEmpty  {
            return nil
        }
    }

    
}
