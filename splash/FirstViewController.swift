//
//  FirstViewController.swift
//  splash
//
//  Created by Nonso Agwuenu
//  Copyright � 2016 Nonso Agwuenu. All rights reserved.
//

import UIKit
import MediaPlayer
import MobileCoreServices
import AssetsLibrary
import PhotosUI
import SwiftyJSON
import Alamofire
import BubbleTransition
import Kingfisher
import SwiftDate
import Haneke


protocol FirstViewControllerDelegate {
    func onOpenComment()
}


class FirstViewController: UIViewController, UITabBarDelegate, UITableViewDataSource, UITableViewDelegate, TableViewCellDelegate, UIGestureRecognizerDelegate, CameraUploadDelegate{
    
    
    
    @IBOutlet weak var centerLabel: UILabel!
    @IBOutlet weak var tableFeed: UITableView!
    var videosComps = [PreviewComp]()
    var selectedVideo:PreviewComp? = PreviewComp()
    let transition = BubbleTransition()
    @IBOutlet weak var loader: UIActivityIndicatorView!
    var  selectedProfile:String?
    var selectedMember:Member?
    var refreshControl: UIRefreshControl!
    
    var numberOfSlidesToShow:Int = 3

    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
    func Camera(post:PreviewComp) {
        
        self.videosComps.insert(post, at: 0)
        
        self.tableFeed.reloadData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
        //self.navigationController?.navigationBar.isTranslucent = true
        
        //self.navigationController?.setNavigationBarHidden(true, animated: false)
        //self.navigationController?.view.backgroundColor = .clear
        //self.tabBarController?.tabBar.tintColor = UIColorFromRGB(rgbValue: 0xa73030)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        self.tableFeed.addSubview(refreshControl)
        
        self.tabBarController?.tabBar.barTintColor = UIColor.black

        //self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 1, height: 1)
      
        
        //self.navigationController?.navigationItem.titleView?.layer.bounds = CGRect(x: 0, y: 0, width: 250, height: 80)
        
//        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Copperplate-Light", size: 24)!]

        self.tableFeed.isHidden = true
        self.centerLabel.isHidden = true
        self.loader.startAnimating()
        self.loader.isHidden = false
        self.loader.hidesWhenStopped = true

        self.tableFeed.delegate = self
        self.tableFeed.dataSource = self
        
        
        self.getPost(){ posts in
            
            var memberId = posts["memberId"].stringValue
            
            self.setVideos(posts)
        }
        
        UIApplication.shared.statusBarStyle = .lightContent
    }


    func onClickComment(commentTag:Int){
        
        self.selectedVideo = self.videosComps[commentTag]
        self.performSegue(withIdentifier: "addComment", sender: self)
    }

    
    func refresh(sender:AnyObject) {
        self.getPost(){ posts in
            self.setVideos(posts)
        }
    }
    
    
    @IBAction func StartShooting(_ sender: UIBarButtonItem) {
        let splashCamera:SplashCamera! = SplashCamera()
        splashCamera.cameraUploadDelegate = self
        splashCamera.openCamera(target:self)
    }
    

    
    func setVideos(_ _friends:JSON){
        
        self.videosComps.removeAll()
        
        for (_,friend) in _friends{
            let posts = friend["friend"]["posts"]
            
            for (_,video) in posts {
                let comp = self.selectedVideo?.instanceFromJSON(video: video)
                self.videosComps.append(comp!)
            }
            
            
        }
        
        self.refreshControl.endRefreshing()
        self.tableFeed.isHidden = false
        self.tableFeed.reloadData()
        self.loader.isHidden = true
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.videosComps.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var cell = tableView.dequeueReusableCell(withIdentifier: "feedCell", for: indexPath) as! TableViewCell

        let video = self.videosComps[(indexPath as NSIndexPath).row]
        self.selectedVideo = video
        
        self.performSegue(withIdentifier: "showPlayer", sender: self)
    }
    
    func changeVideo(timer: Timer!){
        let cell = timer.userInfo as! TableViewCell
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "feedCell", for: indexPath) as! TableViewCell

        let video = self.videosComps[indexPath.row]
        
        cell.addCommentBtn.tag = indexPath.row
        cell.commentCount.text = video.comments.count.description
        cell.thumbnail.hnk_setImage(from: URL(string: video.getThumbnailUrl(memberId: video.memberId!, thumbnailName: video.thumbnailName)))
    
        if let memberId = video.memberId {
            print("MEMBER ID")
            print(memberId)
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.connected(_:)))
        
        cell.delegate = self
        cell.profilePhoto.hnk_setImage(from: URL(string: self.baseUrl(isApi:false) + "videos/" + (video.member?.id)! + "/profile.jpg" ))
        cell.profilePhoto.layer.borderWidth = 3
        cell.profilePhoto.layer.borderColor = UIColor.white.cgColor
        
        
        print("INDEX", indexPath.row)
        cell.profilePhoto.tag = indexPath.row
        cell.profilePhoto.addGestureRecognizer(tapGestureRecognizer)
        
        cell.title.text = video.caption
        cell.numberOfSplashes.text = (video.splashes?.count.description)! + " Splashes"
        cell.numberOfSplashes.text = cell.numberOfSplashes.text?.uppercased()
        cell.timeStamp.text = video.getFriendlyTime()
        cell.timeStamp.text = cell.timeStamp.text?.uppercased()
        cell.username.text = video.member?.username
        
        let utapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.connected(_:)))
        cell.username.tag = indexPath.row
        cell.username.addGestureRecognizer(utapGestureRecognizer)
        cell.username.isUserInteractionEnabled = true
        
        
        return cell
    }
    
    func connected(_ sender:AnyObject){
        self.selectedMember = self.videosComps[(sender.view?.tag)!].member
        self.performSegue(withIdentifier: "postToProfile", sender: self)
        print("you tap image number : \(sender.view.tag)")
    }
    
    @IBAction func thumbnailPro(_ sender: UITapGestureRecognizer) {
        
        print("WORKS!!!")
        
//        self.selectedMember = self.videosComps[(sender.view?.tag)!].member
//        self.performSegue(withIdentifier: "postToProfile", sender: self)
        
    }
    

    func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        print("foo bar")
        
    }
    
    func getPost( _ completion:@escaping (_ res:JSON)->Void ){
        
        let params = [
            "filter":[
                "include":[["friend":["posts":["comments","member", "splashes" ]]]],
                "order":[
                    "createdAt DESC"
                ]
            ]
        ]
        
        let url:String = self.baseUrl() + "/Members/" + self.getSession()["userId"].stringValue + "/friends"
        
        Alamofire.request(url, parameters: params)
            .response(
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    
                    // Validate your JSON response and convert into model objects if necessary
                    
                    if let res = response.result.value {
                        let json = JSON(res)
                        
                        print(json)
                  
                        let error = json["error"]
                        
                        // ..2.. If any errors, handle the error
                        if error != nil {
                            _ = self.httpErrorHandler(err: res as AnyObject)
                            return
                        }

                        completion(json)
        
                        
                    }

            }
        )}
    
    
    func getPostById(id:String, _ completion:@escaping (_ res:JSON)->Void ){
        
        let params = [
            "filter":[
                "include":[["friend":["posts":["comments","member", "splashes" ]]]],
                "order":[
                    "createdAt DESC"
                ]
            ]
        ]
        
        let url:String = self.baseUrl() + "/Members/" + self.getSession()["userId"].stringValue + "/friends/" + id
        
        Alamofire.request(url, parameters: params)
            .response(
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    
                    // Validate your JSON response and convert into model objects if necessary
                    
                    if let res = response.result.value {
                        let json = JSON(res)
                        print("POST")
                        print(json)
                        let error = json["error"]
                        
                        // ..2.. If any errors, handle the error
                        if error != nil {
                            _ = self.httpErrorHandler(err: res as AnyObject)
                            return
                        }
                        
                        completion(json)
                        
                    }
                    
            }
        )}
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addComment" {
            let postPlayer:CommentViewController = segue.destination as! CommentViewController
            postPlayer.video = self.selectedVideo
        }
        if segue.identifier == "showPlayer"{
            let nav:UINavigationController = segue.destination as! UINavigationController
            let postPlayer:PostPlayerViewController = nav.topViewController as! PostPlayerViewController
            postPlayer.previewComp = self.selectedVideo
        }
        if segue.identifier == "postToProfile"{
            let profileVC:ProfileViewController = segue.destination as! ProfileViewController
            profileVC.member = self.selectedMember
        }
    }

}

func UIColorFromRGB(rgbValue: UInt) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}






