//
//  PreviewViewController.swift
//  splash
//
//  Created by Christopher Kendrick on 6/10/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit
import MediaPlayer
import Alamofire
import SwiftyJSON
import AVKit

protocol postVideoDelegate {
    func onPost()
}

class PreviewViewController: UIViewController {
    
    var videoUrl:URL!
    var caption:String?
    var thumb:UIImage?
    var player:AVPlayerViewController?
    
    var previewComp:PreviewComp! = nil

    
    var moviePlayer: AVPlayerViewController?
    var delegate:postVideoDelegate?
    var isSplashing:Bool? = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playVideo(videoUrl)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func didCancel(_ previewOverlay:PreviewOverlay) {
        //moviePlayer?.stop()
        self.dismiss(animated: true, completion: nil)
    }
    
    func didPost(_ previewOverlay:PreviewOverlay, caption:String){
        let id = self.getSession()["userId"].stringValue
        if isSplashing == false {
            self.uploadVideo(self.videoUrl, apiUrl: self.baseUrl() + "Videos" + "/" + id + "/upload", caption: caption)
        }else {
            print("splashing")
            self.uploadVideo(self.videoUrl, apiUrl: self.baseUrl() + "Videos" + "/" + id + "/upload", caption: caption)
        }
    }
    
    
    func uploadVideo(_ fileUrl:URL, apiUrl:String, caption:String){
//        let session = self.getSession()
//        let userId = session["userId"].stringValue
//        self.baseUrl() + "Videos/" + userId + "/upload"
//
        
        Alamofire.upload(
            //to:apiUrl,
            //method:.post,
            multipartFormData: { multipartFormData in
                multipartFormData.append(caption.data(using: String.Encoding.utf8)!, withName: "title")
                multipartFormData.append(fileUrl, withName: "video")
                if self.isSplashing == true {
                    multipartFormData.append("true".data(using: String.Encoding.utf8)!, withName: "isSplashing")
                    multipartFormData.append(self.previewComp.id.data(using: String.Encoding.utf8)!, withName: "postId")
                }
            },
            to:apiUrl,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        //completion(response:response)
                        
                        self.performSegue(withIdentifier: "fb", sender: nil)
                        
                        //self.delegate?.onPost()
                        //self.dismissViewControllerAnimated(true, completion: nil)
                        debugPrint(response)
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
            }
        )
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.delegate?.onPost()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func playVideo(_ videoUrl:URL) {
        let player = AVPlayer(url: videoUrl)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
        
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("*********************vdvdv dvds")
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }


}
