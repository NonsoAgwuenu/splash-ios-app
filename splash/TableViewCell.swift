//
//  TableViewCell.swift
//  splash
//
//  Created by Christopher Kendrick on 6/30/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol TableViewCellDelegate {
    func onClickComment(commentTag:Int)
}

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var numberOfSplashes: UILabel!
    @IBOutlet weak var timeStamp: UILabel!
    @IBOutlet weak var addCommentBtn: UIButton!
    @IBOutlet weak var commentCount: UILabel!
    
    var thumbnailUrls = [String]()
    var splashes = [JSON]()
    var splashesInst = [PreviewComp]()
    var timer:Timer! = nil
    var currentThumbnailIndex:Int =  0
    var delegate:TableViewCellDelegate? = nil
    var thumbnailIndex:Int = 1
    
    override func prepareForReuse() {
        super.prepareForReuse()
        thumbnail.image = nil
    }

    @IBAction func addComment(_ sender: UIButton) {
        self.delegate?.onClickComment(commentTag:self.addCommentBtn.tag)
    }
    
    func changeVideo(video:PreviewComp){
            print(video.getVideoThumbnails())
    }
    
    
    
    func changeImgTimer(_ timer:Timer){
        //self.thumbnail.image = video.getThumbnail(url: video.getVideoThumbnails()[0])
        var comp = timer.userInfo as! PreviewComp
        
        if self.thumbnailIndex >= comp.getVideoThumbnails().count {
            self.thumbnailIndex = 0
        }
        
        let imgUrl = comp.getVideoThumbnails()[self.thumbnailIndex]
        
        
        let photoUrl = NSURL(string:imgUrl)
        if photoUrl != nil {
            self.thumbnail.image =  UIImage(data: NSData(contentsOf: photoUrl as! URL)! as Data)!
        }
        

        print("Change Thumbnail to:", thumbnailIndex)
        
        self.thumbnailIndex = self.thumbnailIndex + 1
        
        
    }
    
    
    
    func startCellTimer(vid:PreviewComp){
        self.timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(changeImgTimer), userInfo: vid, repeats: true)
        self.timer.fire()
    }
    
    func setThumbs(comp:PreviewComp){
        self.splashesInst.append(comp)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        thumbnail.clipsToBounds = true
        
        profilePhoto.layer.cornerRadius = self.profilePhoto.layer.bounds.width / 2
        profilePhoto.layer.masksToBounds = true
        profilePhoto.layer.borderColor = UIColor.lightGray.cgColor
        
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
