//
//  NotificationsViewController.swift
//  splash
//
//  Created by Christopher Kendrick on 1/26/17.
//  Copyright © 2017 Christopher Kendrick. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

public class Notification: NSObject {
    let id:String
    let title:String
    let createdAt:String
    let sender:Member
    let friendId:String
    let type:String
    let payload:Any
    
    init(id:String = "", title:String = "", createdAt:String = "", friendId:String = "", sender:Member = Member(), type:String = "", payload:Any = []){
        self.id = id
        self.title = title
        self.createdAt = createdAt
        self.sender = sender
        self.friendId = friendId
        self.type = type
        self.payload = payload
    }
    
    
    func instanceFromJSON(notification:JSON)->Notification{
        let id = notification["id"].stringValue
        let title = notification["name"].stringValue
        let createdAt = notification["createdAt"].stringValue
        let friendId = notification["friendId"].stringValue
        let type = notification["type"].stringValue
        let sender = notification["friend"]
        let payload = notification["payload"]
        let senderMember = Member().instanceFromJSON(member: sender)
        return Notification(id: id, title: title, createdAt: createdAt, friendId:friendId, sender:senderMember, type:type, payload:payload)
    }
    
}

class NotificationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var notificationsArr = [Notification]()
    var selectedRow:Notification? = nil
    var selectedVideo:PreviewComp? = PreviewComp()
    var selectedSplashId:String?
    
    @IBOutlet weak var tbl: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tbl.delegate = self
        self.tbl.dataSource = self
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        notificationsArr = [Notification]()
        self.getNotifications(){ notifications in
            print(notifications)
            for notif in notifications{
                let n = Notification()
                let newNotif = n.instanceFromJSON(notification: notif.1)
                self.notificationsArr.append(newNotif)
            }
            self.tbl.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.notificationsArr.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationTableViewCell
        
        let n = self.notificationsArr[indexPath.row]
        cell.title.text = n.title
        cell.thumbnail.hnk_setImage(from: URL(string: self.baseUrl(isApi:false) + "videos/" + n.friendId + "/profile.jpg" ))
        // Configure the cell...
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedRow = self.notificationsArr[indexPath.row]
        
        switch self.selectedRow!.type {
        case "splash":
            
            if let pl = self.selectedRow?.payload as? JSON {
                let splashId = pl["splashId"].stringValue
                let postId = pl["postId"].stringValue

                self.getPostById(id: postId){ post in
                    self.selectedVideo = self.selectedVideo?.instanceFromJSON(video: post[0])
                    self.selectedSplashId = splashId
                    self.performSegue(withIdentifier: "previewNotification", sender: self)
                }
            }
            
            //print(self.selectedRow?.payload)
            
        case "friend":
            self.performSegue(withIdentifier: "toProfile", sender: self)
        default:
            self.performSegue(withIdentifier: "toProfile", sender: self)
        }
    }
    
    func getNotifications( _ completion:@escaping (_ res:JSON)->Void ){
        
        let accessToken = self.getSession()["id"].stringValue
        let headers = [
            "Authorization":accessToken
        ]
        
        let params = [
            "filter":[
                "include":["friend"],
                "order":[
                    "createdAt DESC"
                ]
            ]
        ]
        
        let url:String = self.baseUrl() + "/Members/" + self.getSession()["userId"].stringValue + "/notifications"
        
        Alamofire.request(url, method:.get, parameters: params, headers:headers)
            .response(
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    
                    // Validate your JSON response and convert into model objects if necessary
                    
                    if let res = response.result.value {
                        let json = JSON(res)
                      
                        let error = json["error"]
                        
                        // ..2.. If any errors, handle the error
                        if error != nil {
                            _ = self.httpErrorHandler(err: res as AnyObject)
                            return
                        }
                        
                        completion(json)
                        
                    }
                    
            }
        )}
    
    
    func getPostById(id:String, _ completion:@escaping (_ res:JSON)->Void ){
        
        let accessToken = self.getSession()["id"].stringValue
        let headers = [
            "Authorization":accessToken
        ]
        
        let params = [
            "filter":[
                "where":["id":id],
                "include":["comments","member","splashes"],
                "order":[
                    "createdAt DESC"
                ]
            ]
        ]
        
        let url:String = self.baseUrl() + "/Members/" + self.getSession()["userId"].stringValue + "/posts"
        
        Alamofire.request(url, parameters: params, headers:headers)
            .response(
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    
                    // Validate your JSON response and convert into model objects if necessary
                    
                    if let res = response.result.value {
                        let json = JSON(res)

                        let error = json["error"]
                        
                        // ..2.. If any errors, handle the error
                        if error != nil {
                            _ = self.httpErrorHandler(err: res as AnyObject)
                            return
                        }
                        
                        completion(json)
                        
                        
                    }
                    
            }
        )}
    
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toProfile" {
            let profileVC:ProfileViewController = segue.destination as! ProfileViewController
            profileVC.member = self.selectedRow?.sender
            
        }
        if segue.identifier == "previewNotification"{
            let nav:UINavigationController = segue.destination as! UINavigationController
            let postPlayer:PostPlayerViewController = nav.topViewController as! PostPlayerViewController
            postPlayer.previewComp = self.selectedVideo
            postPlayer.startAtSplashId = self.selectedSplashId!
        }
    }


}
