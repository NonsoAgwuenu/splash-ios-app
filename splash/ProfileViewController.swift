//
//  ProfileViewController.swift
//  splash
//
//  Created by Christopher Kendrick on 10/21/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit
import MediaPlayer
import Alamofire
import SwiftyJSON


class ProfileViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    @IBOutlet weak var followBtn: UIButton!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var friendCount: UILabel!
    @IBOutlet weak var bioText: UITextView!
    @IBOutlet weak var postsCollectionView: UICollectionView!
    @IBOutlet weak var shadowView: UIView!
    
    var refreshControl: UIRefreshControl!

    var member:Member? = nil
    var posts = [PreviewComp]()
    var selectedPost:PreviewComp? = nil
    var isFollowing:Bool = false
    
    var memberprofileId:String = ""
    var friendMapperId:String = ""
    var canEdit:Bool = false
    @IBOutlet weak var followersCount: UILabel!
    @IBOutlet weak var fullname: UILabel!
    var whoUserFollowingType:Bool = false
    
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    @IBOutlet weak var follCount: UILabel!
    
    var profileId:String?
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = ""
        self.posts.removeAll()
        self.getPost(){ posts in
            for (_,video) in posts {
                var p:PreviewComp = PreviewComp()!
                let comp = p.instanceFromJSON(video: video)
                self.posts.append(comp)
            }
            self.setVideoCount()
            self.postsCollectionView.reloadData()
        }
        self.getUserProfile(){ member in
            self.setUserView(member: member)
        }
        self.navigationController?.setToolbarHidden(true, animated: false)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationBar.topItem.title = "some title"
        
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        self.postsCollectionView.addSubview(refreshControl)
        
       
        
        self.profilePic.layer.cornerRadius = self.profilePic.bounds.width/2
        self.profilePic.clipsToBounds = true
        self.profilePic.layer.borderColor = UIColor.lightGray.cgColor
        self.profilePic.layer.borderWidth = 0.5
        
        self.followBtn.setTitle("loading...", for: .disabled)
        self.followBtn.isEnabled = false
        self.followBtn.layer.cornerRadius = 2
        self.followBtn.layer.borderColor = UIColor.lightGray.cgColor
        self.followBtn.layer.borderWidth = 0
        self.followBtn.layer.borderWidth = 0.5
        self.followBtn.layer.borderColor = UIColor.black.cgColor
        
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 0.3
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        shadowView.layer.shadowRadius = 2
        
        self.postsCollectionView.dataSource = self
        self.postsCollectionView.delegate = self
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 1
        
        self.postsCollectionView.collectionViewLayout = layout
        self.postsCollectionView.layer.borderWidth = 0
        
//        self.posts.removeAll()
//        
//        // Do any additional setup after loading the view.
//        self.getPost(){ posts in
//            for (_,video) in posts {
//                var p:PreviewComp = PreviewComp()!
//                let comp = p.instanceFromJSON(video: video)
//                self.posts.append(comp)
//            }
//            self.setVideoCount()
//            self.postsCollectionView.reloadData()
//        }
//        self.getUserProfile(){ member in
//            self.setUserView(member: member)
//        }
        self.friends(){ friends in
            if friends.count > 0 {
                self.followersCount.text = String(friends.count)
            }
        }
        self.friendsFoll(){ fr in
            self.follCount.text = String(fr.count)
        }
        // Check if they;re friends
        
        
    }
    @IBAction func showFoll(_ sender: UITapGestureRecognizer) {
        self.whoUserFollowingType = true
        self.performSegue(withIdentifier: "showFollow", sender: self)
    }
    
    func setVideoCount(){
        self.friendCount.text = String(self.posts.count)
    }
    
    func friends( _ completion:@escaping (_ res:JSON)->Void ){
        
        let accessToken = self.getSession()["id"].stringValue
        let headers = [
            "Authorization":accessToken
        ]
        
        let params = [
            "filter":[
                "include":"member"
            ]
        ]
        
        var memberId = self.member?.id
        
        if self.member == nil {
            memberId = self.getSession()["userId"].stringValue
        }
        
        Alamofire.request(self.baseUrl() + "/Members/" + memberId! + "/incomingFriendRequest", method:.get, parameters:params, headers:headers)
            .response(
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    
                    // Validate your JSON response and convert into model objects if necessary
                    
                    if let res = response.result.value {
                        let json = JSON(res)
                        let error = json["error"]
                        
                        // ..2.. If any errors, handle the error
                        if error != nil {
                            _ = self.httpErrorHandler(err: res as AnyObject)
                            return
                        }
                        
                        completion(json)
                        
                    }
                    
            }
    )}
    
    
    func friendsFoll( _ completion:@escaping (_ res:JSON)->Void ){
        
        let accessToken = self.getSession()["id"].stringValue
        let headers = [
            "Authorization":accessToken
        ]
        
        let params = [
            "filter":[
                "include":"member"
            ]
        ]
        
        var memberId = self.member?.id
        
        if self.member == nil {
            memberId = self.getSession()["userId"].stringValue
        }
        
        Alamofire.request(self.baseUrl() + "/Members/" + memberId! + "/friends", method:.get, parameters:params, headers:headers)
            .response(
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    
                    // Validate your JSON response and convert into model objects if necessary
                    
                    if let res = response.result.value {
                        let json = JSON(res)
                        let error = json["error"]
                        
                        // ..2.. If any errors, handle the error
                        if error != nil {
                            _ = self.httpErrorHandler(err: res as AnyObject)
                            return
                        }
                        
                        completion(json)
                        
                    }
                    
            }
        )}
    
    
    @IBAction func showfollowing(_ sender: UITapGestureRecognizer) {
        self.whoUserFollowingType = false
        self.performSegue(withIdentifier: "showFollow", sender: self)
    }
    
    func isFollowing(isFollowMember:String, sessionMemberId:String, completion:@escaping (_ count:JSON)->Void )->Bool{
        
        let accessToken = self.getSession()["id"].stringValue
        let headers = [
            "Authorization":accessToken
        ]
        let params = [
            "filter":[
                "where":[
                    "friendId":isFollowMember
                ]
            ]
        ]

        Alamofire.request(self.baseUrl() + "/Members/" + sessionMemberId + "/friends" , method:.get, parameters:params, headers:headers)
            .response(
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    
                    if let res = response.result.value {
                        let json = JSON(res)
                        let error = json["error"]
                        
                        // ..2.. If any errors, handle the error
                        if error != nil {
                            _ = self.httpErrorHandler(err: res as AnyObject)
                            return
                        }
                        
                        completion(json)
                    }
                    
            })
        
        return false
    }
    
    
    func refresh(){
    
    }
    
    func setUserView(member:JSON){
        //self.navigationController?.topViewController?.title = member["username"].stringValue
        //self.title = member["username"].stringValue
        self.title = member["username"].stringValue
        self.bioText.text = member["bio"].stringValue
        self.fullname.text = member["firstname"].stringValue + " " + member["lastname"].stringValue
        
        if self.member == nil && self.profileId == nil {
             self.profilePic.hnk_setImage(from: URL(string: self.baseUrl(isApi:false) + "videos/" + self.getSession()["userId"].stringValue + "/profile.jpg" ))
        }else if self.member != nil{
             self.profilePic.hnk_setImage(from: URL(string: self.baseUrl(isApi:false) + "videos/" + (self.member?.id)! + "/profile.jpg" ))
        }else{
            print("PROFILE IDDDDDD************")
            self.profilePic.hnk_setImage(from: URL(string: self.baseUrl(isApi:false) + "videos/" + self.profileId! + "/profile.jpg" ))
        }
        
        self.memberprofileId = member["id"].stringValue
        
        
        if(self.memberprofileId == self.getSession()["userId"].stringValue){
            self.canEdit = true
            self.isFollowing = false
            self.followBtn.setTitle("EDIT PROFILE", for: .normal)
            self.followBtn.backgroundColor = .white
            self.followBtn.setTitleColor(.black, for: .normal)
            self.followBtn.isEnabled = true
        }else{
            self.isFollowing(isFollowMember: self.memberprofileId, sessionMemberId: self.getSession()["userId"].stringValue){ res in
                let foundMember = res[0]
                if foundMember != nil {
                    self.friendMapperId = foundMember["id"].stringValue
                    self.isFollowing = true
                    self.followBtn.setTitle("FOLLOWING", for: .normal)
                    self.followBtn.backgroundColor = .white
                    self.followBtn.setTitleColor(.black, for: .normal)
                }else{
                    
                }
                
                self.followBtn.isEnabled = true
                
            }
        }
        

    }
    
    
    func getUserProfile( completion:@escaping ( _ res:JSON )->Void ) {
        var session = self.getSession()
        let headers = [
            "Authorization":session["id"].stringValue
        ]
        
        var memberId = session["userId"].stringValue
        
        if self.member != nil {
            memberId = (self.member?.id)!
        }
        if self.profileId != nil {
            memberId = self.profileId!
        }
        
        let url = self.baseUrl() + "Members/findOne"
        
        let params = [
            "filter":[
                "where":[
                    "id":memberId
                ]
            ]
        ]
        
       

        Alamofire.request(url, method:.get, parameters:params, headers: headers)
            .responseJSON { response in
                
                print(response)
                
                if let res = response.result.value {
                    let json = JSON(res)
                    
                    let error = json["error"]
                    
                    // ..2.. If any errors, handle the error
                    if error != nil {
                        self.httpErrorHandler(err: res as AnyObject)
                        return
                    }
                    
                    completion(json)
                }
        }
    }
    
    
    func setPreviewCompFromJson(_ videos:JSON){
        
        self.posts = []
        
        for video in videos {
            let id = video.1["id"].stringValue
            let title = video.1["title"].stringValue
            let url:String = self.baseUrl(isApi: false) + video.1["videoUrl"].stringValue
            let urlS = url.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            
            let videoUrl = NSURL(string: urlS!, relativeTo:nil)
            
            //let thumb = self.thumbnailForVideoAtURL(videoUrl! as URL)
            
            let splashes = video.1["splashes"].arrayValue
            
            let updated:String = video.1["updatedAt"].stringValue
            let created:String = video.1["createdAt"].stringValue
            
            let member = Member(id: "sdvs", lastname: "sdvsd", firstname: "sdvsd", bio: "vsdvsd", username: "sdvdsv", email: "sdvsdvd")
            
            let v = PreviewComp(id:id, caption:title, videoUrl:videoUrl as! URL, thumbnail: nil, splashes: splashes, member:member, createdAt:created, updatedAt:updated)
            
            self.posts.append(v!)

        }
        
        self.postsCollectionView.reloadData()
    }
    
    fileprivate func thumbnailForVideoAtURL(_ url: URL) -> UIImage? {
        
        let asset = AVAsset(url: url)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print("error")
            return nil
        }
    }
    
    func unfollowMember(friendId:String, completion:@escaping (_ res:JSON)->Void ){
        var session = self.getSession()
        let headers = [
            "Authorization":session["id"].stringValue
        ]
        let memberId = session["userId"].stringValue
        
        // ..1.. Make request to login Api
        Alamofire.request(self.baseUrl() + "Members/" + memberId + "/friends/" + friendId, method:.delete, headers:headers)
            .responseJSON { response in
                print(response)
                if let res = response.result.value {
                    let json = JSON(res)
                    let error = json["error"]
                    
                    // ..2.. If any errors, handle the error
                    if error != nil {
                        _ = self.httpErrorHandler(err: res as AnyObject)
                        return
                    }
                    
                    completion(json)
                    
                }
        }
    }
    
    func followMember( _ completion:@escaping (_ res:JSON)->Void ){
        
        var session = self.getSession()
        let headers = [
            "Authorization":session["id"].stringValue
        ]
        let memberId = session["userId"].stringValue
        
        let params = [
            "memberId":memberId,
            "friendId":self.member!.id,
            "accepted":false
            ] as [String : Any]
        
        
        // ..1.. Make request to login Api
        Alamofire.request(self.baseUrl() + "Members/" + memberId + "/friends", method:.post, parameters: params, headers:headers)
            .responseJSON { response in
                
                print(response)
                
                if let res = response.result.value {
                    let json = JSON(res)
                    let error = json["error"]
                    
                    // ..2.. If any errors, handle the error
                    if error != nil {
                        _ = self.httpErrorHandler(err: res as AnyObject)
                        return
                    }
                    
                    completion(json)
                    
                }
        }
    }
    
    
    
    func getPost( _ completion:@escaping (_ res:JSON)->Void ){
        var session = self.getSession()
        let headers = [
            "Authorization":session["id"].stringValue
        ]
        var memberId = session["userId"].stringValue
        
        let params = [
            "filter":[
                "include":[ "member", ["splashes":["member"] ], "comments" ],
                "order":"createdAt DESC"
            ]
        ]
        
        if self.member != nil {
            memberId = (member?.id)!
        }
        
        
        if self.profileId != nil {
            memberId = self.profileId!
            print("profile id", memberId)
        }
        
        // ..1.. Make request to login Api
        Alamofire.request(self.baseUrl() + "Members/" + memberId + "/posts", method:.get, parameters: params, headers:headers)
            .responseJSON { response in
                if let res = response.result.value {
                    let json = JSON(res)
                    let error = json["error"]
                    
                    // ..2.. If any errors, handle the error
                    if error != nil {
                        _ = self.httpErrorHandler(err: res as AnyObject)
                        return
                    }
                    
                    completion(json)
                    
                }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func followBtnClick(_ sender: UIButton) {
        
        
        
        if self.canEdit {
            self.performSegue(withIdentifier: "toEditProfile", sender: self)
        }else{
            self.followBtn.isEnabled = false
            if self.isFollowing {
                print("UNFOLLOWING MEMBER")
                self.followBtn.setTitle("unfollowing...", for: .normal)
                self.unfollowMember(friendId: self.friendMapperId){ count in
                    self.isFollowing = false
                    self.followBtn.isEnabled = true
                    self.followBtn.setTitle("FOLLOW", for: .normal)
                    self.followBtn.backgroundColor = .black
                    self.followBtn.setTitleColor(.white, for: .normal)
                }
            }else{
                print("FOLLOWING MEMBER")
                self.followBtn.setTitle("following...", for: .normal)
                self.followMember(){ friends in
                    
                    let friendMapperId = friends["id"].stringValue
                    self.friendMapperId = friendMapperId
                    
                    self.isFollowing = true
                    self.followBtn.isEnabled = true
                    self.followBtn.setTitle("FOLLOWING", for: .normal)
                    self.followBtn.backgroundColor = .white
                    self.followBtn.setTitleColor(.black, for: .normal)
                    
                }
            }
        }
        

    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("number items in collection:", self.posts.count)
        return self.posts.count
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "postCell", for: indexPath) as! GridUICollectionViewCell
        
        let post = self.posts[indexPath.row]
        
      
        cell.thumbnail.hnk_setImage(from: URL(string: post.getThumbnailUrl(memberId: post.memberId!, thumbnailName: post.thumbnailName)))
        
        
        cell.title.text = post.caption
        if (post.splashes != nil){
             cell.splashCount.text = String(post.splashes!.count)
        }
       
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("tap")
        self.selectedPost = self.posts[(indexPath as NSIndexPath).row]
        self.performSegue(withIdentifier: "profileToPlayer", sender: self)
    }

    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        var w = self.postsCollectionView.bounds.width - 1
        return CGSize(width:w/2, height:180)
    }
    
    override func viewWillLayoutSubviews() {
        self.postsCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "profileToPlayer" {
            let nav:UINavigationController = segue.destination as! UINavigationController
            let postPlayer:PostPlayerViewController = nav.topViewController as! PostPlayerViewController
            if self.selectedPost != nil {
                postPlayer.previewComp = self.selectedPost
            }
        }
        if segue.identifier == "showFollow" {
            //let friendList:UINavigationController = segue.destination as! UINavigationController
            let vc:FriendViewController = segue.destination as! FriendViewController
            vc.member = self.member
            vc.whoUserFollowingType = self.whoUserFollowingType
            
        }
        
    }
    
    func getFollowing(memberId:String, completion:@escaping (_ frineds:JSON)->Void){
        var session = self.getSession()
        let headers = [
            "Authorization":session["id"].stringValue
        ]
        var memberId = session["userId"].stringValue

        
        if self.member != nil {
                memberId = (self.member?.id)!
        }
        
        // ..1.. Make request to login Api
        Alamofire.request(self.baseUrl() + "Members/" + memberId + "/incomingFriendRequest", method:.get, headers:headers)
            .responseJSON { response in
                
                print(response)
                
                if let res = response.result.value {
                    let json = JSON(res)
                    let error = json["error"]
                    
                    // ..2.. If any errors, handle the error
                    if error != nil {
                        _ = self.httpErrorHandler(err: res as AnyObject)
                        return
                    }
                    
                    completion(json)
                    
                }
        }
    }
    
    

 

}
