//
//  PreviewComp.swift
//  splash
//
//  Created by Christopher Kendrick on 10/4/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import SwiftDate
import AVFoundation


class PreviewComp {
    
    var id:String!
    var caption:String!
    var videoUrl:URL!
    var thumbnail:UIImage!
    var splashes:[JSON]?
    var createdAt:String!
    var updatedAt:String!
    var numberOfSplashes:Int!
    var thumbnailName:String!
    var member:Member? = nil
    var memberId:String? = nil
    var comments = [Comment]()
    var isAccepted:Bool
    init?(id:String? = "", caption:String? = "", videoUrl:URL? = URL(fileURLWithPath: ""), thumbnail:UIImage? = UIImage(), splashes:[JSON]? = [], member:Member? =  nil, createdAt:String? = "", updatedAt:String =  "", numberOfSplashes:Int? = 0, thumbnailName:String = "", memberId:String? = "", comments:[Comment] = [Comment](), isAccepted:Bool = false){
        
        self.id = id
        self.caption = caption
        self.videoUrl = videoUrl
        self.thumbnail = thumbnail
        self.splashes = splashes
        self.member = member
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.numberOfSplashes = numberOfSplashes
        self.thumbnailName = thumbnailName
        self.thumbnail = thumbnail
        self.memberId = memberId
        self.comments = comments
        self.isAccepted = isAccepted
    }
    
    func getThumbnailUrl(memberId:String, thumbnailName:String)->String {
        return UIViewController().baseUrl(isApi: false) + "videos/" + memberId + "/" + thumbnailName + "-thumbnail-0.jpg"
    }
    
    
    func getFriendlyTime()->String {
        let date1 = try! DateInRegion(string: createdAt!, format: .custom("yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"), fromRegion: nil)
        let datFormatted = try! date1.colloquialSinceNow().colloquial
        return datFormatted
    }
    
    
    func getVideoThumbnails() -> [String] {
        return [
            self.getThumbUrl(id: "0"),
            self.getThumbUrl(id: "1"),
            self.getThumbUrl(id: "2")
        ]
    }
    
    
    func getThumbUrl(id:String)->String {
        return UIViewController().baseUrl(isApi: false) + "videos/" + (self.member?.id)! + "/" + "" + self.thumbnailName + "-thumbnail" + "-" + id + ".jpg"
    }


    func getThumbnail(url:String)->UIImage{
        let photoUrl = NSURL(string:url)
        if photoUrl != nil {
            self.thumbnail =  UIImage(data: NSData(contentsOf: photoUrl as! URL)! as Data)!
        }
        
        return self.thumbnail
    }
    
    func instanceFromJSON(video:JSON)->PreviewComp{
        
        // Post Info
        let id = video["id"].string
        let title = video["title"].string
        let createdAt = video["createdAt"].string
        
        let thumbnailName = video["thumbnailUrl"].string
        
        let url:String = UIViewController().baseUrl(isApi: false) + video["videoUrl"].string!
        let videoUrl:NSURL = NSURL(string: url)!
        
        let memberInstance = video["member"]
        
        let memberId = video["memberId"].stringValue
        
        let m:Member = Member()
        let member = m.instanceFromJSON(member: memberInstance)
        
        // Post Splashes
        let splashes = video["splashes"].arrayValue
        // Video Url
        
        
        let comments = video["comments"].arrayValue
        
        
        let c = Comment().instanceArrFromJSONArr(comments: comments)
        
        let _preview = PreviewComp(id: id!, caption: title!, videoUrl: videoUrl as URL, splashes: splashes, member: member, createdAt:createdAt, updatedAt: "", thumbnailName: thumbnailName!, memberId:memberId, comments:c)
        
        return _preview!

    }
    
    
    
}



extension PreviewComp {
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
}

class Member {
    
    var id:String!
    var lastname:String!
    var firstname:String!
    var bio:String!
    var username:String!
    var email:String!
    var profileImage:UIImage?
    
    init(id:String = "", lastname:String = "", firstname:String = "", bio:String = "", username:String = "", email:String = "", profileImage:UIImage = UIImage()){
        
        self.id = id
        self.lastname = lastname
        self.firstname = firstname
        self.bio = bio
        self.username = username
        self.email = email
        self.profileImage = profileImage
    }
    
    func instanceFromJSON(member:JSON)->Member{
        let memberId = member["id"].stringValue
        let username = member["username"].stringValue
        return Member(id: memberId, username: username)
        
    }
    
    
    func getProfileImage()->UIImage{
        
        let profilePhotoUrlString = UIViewController().baseUrl(isApi: false) + "videos/" + self.id + "/profile.jpg"
        
        let photoUrl = NSURL(string:profilePhotoUrlString)
        if photoUrl != nil {
            self.profileImage =  UIImage(data: NSData(contentsOf: photoUrl as! URL)! as Data)!
        }
        
        return self.profileImage!
    }
    
    func setFromArray(member:JSON) {
        
//        let member = Member(id: member.id, lastname: member.lastname, firstname: member.firstname, bio: member.bio, username: member.username, email: member.email, company: member.email)
//        return member
    }
    
    func remoteFileExists(url: String) -> Bool {
        
        var exists: Bool = false
        let url: NSURL = NSURL(string: url)!
        var request: NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "HEAD"
        var response: URLResponse?
        
        do{
            try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response)
        }catch{
            print("error")
        }
        
        if let httpResponse = response as? HTTPURLResponse {
            
            if httpResponse.statusCode == 200 {
                
                exists =  true
            }else{
                exists  = false
            }
            
        }
        
        return exists
    }
    
}
