//
//  SignupViewController.swift
//  splash
//
//  Created by Christopher Kendrick on 7/12/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON



class SignupViewController: UIViewController {
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func signup() {
        
        let email = self.email.text! as String
        let password = self.password.text! as String
        let username = self.username.text! as String
        
        let params = ["email":email,"password":password, "username":username]
        
        // ..1.. Make request to login Api
        Alamofire.request(self.baseUrl() + "Members", method:.post, parameters: params)
            .responseJSON { response in
                if let res = response.result.value {
                    let json = JSON(res)
                    let error = json["error"]
                    
                    // ..2.. If any errors, handle the error
                    if error != nil {
                        self.httpErrorHandler(err: res as AnyObject)
                        return
                    }
                    
                    // ..3.. If the signup was successful go to the login page.
                    self.goToPage("Authentication", viewControllerName: "login")
                }
        }
    }
    
    @IBAction func createUser(_ sender: UIButton) {
        self.signup()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
