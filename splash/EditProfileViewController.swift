//
//  EditProfileViewController.swift
//  splash
//
//  Created by Christopher Kendrick on 10/21/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ImageIO

class EditProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var companyName: UITextField!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var bio: UITextField!
    
    @IBOutlet weak var thumbnail: UIImageView!
    
    var selectedImage:UIImage? = nil
    var selectedImageUrl:URL? = URL(fileURLWithPath: "")
    var didProfilePhotoChange:Bool! = false
    
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imagePicker.delegate = self
        
        self.thumbnail.layer.cornerRadius = self.thumbnail.frame.size.width / 2;
        self.thumbnail.clipsToBounds = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showImagePicker))
        self.thumbnail.addGestureRecognizer(tapGesture)
        self.thumbnail.isUserInteractionEnabled = true
        
        self.getProfile(){ member in
            print("member!!!!")
            print(member)
            self.setProfileTextFields(member: member)
        }
        
        // Do any additional setup after loading the view.
    }
    
    func showImagePicker(gestureRecognizer: AnyObject){
        present(self.imagePicker, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        let imageUrl          = info[UIImagePickerControllerReferenceURL] as? NSURL
        let imageName         = "profile.jpg"
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let photoURL          = NSURL(fileURLWithPath: documentDirectory)
        let localPath         = photoURL.appendingPathComponent(imageName)
        
        let resizedImage = self.resizeImage(image: image, targetSize: CGSize(width: 100, height: 100))
        
        do {
            try UIImageJPEGRepresentation(resizedImage, 1.0)?.write(to: localPath!)
            self.thumbnail.image = resizedImage
            self.didProfilePhotoChange = true
            self.selectedImage = resizedImage
            self.selectedImageUrl = localPath
        }catch {
            print("error saving file")
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    
    @IBAction func saveProfile(_ sender: UIButton) {
        
        var session = self.getSession()
        
        let memberId = session["userId"].stringValue
        let url = self.baseUrl() + "Videos/" + memberId + "/upload"
        

        
        self.saveProfile(){ member in
            if self.didProfilePhotoChange == true{
                self.uploadPreview(endPoint: url, image: self.selectedImageUrl!, uploadPhoto: true){ upload in
                    self.navigationController?.popViewController(animated: true)
                }
            }else{
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func setProfileTextFields(member:JSON){
        self.email.text = member["email"].stringValue
        self.firstName.text = member["firstname"].stringValue
        self.lastName.text = member["lastname"].stringValue
        self.companyName.text = member["company"].stringValue
        self.bio.text = member["bio"].stringValue
        
        var session = self.getSession()
        
        let memberId = session["userId"].stringValue
        let url = self.baseUrl() + "Videos/" + memberId + "/upload"
        
        let profilePhotoUrlString = self.baseUrl(isApi: false) + "videos/" + memberId + "/profile.jpg"
        
        if self.remoteFileExists(url: profilePhotoUrlString) {
            let photoUrl = NSURL(string:profilePhotoUrlString)
            if photoUrl != nil {
                self.thumbnail.image =  UIImage(data: NSData(contentsOf: photoUrl as! URL)! as Data)
            }
        }
    }
    
    func remoteFileExists(url: String) -> Bool {
        
        var exists: Bool = false
        let url: NSURL = NSURL(string: url)!
        var request: NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "HEAD"
        var response: URLResponse?
        do{
            try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response)
        }catch{
            print("error")
        }
        
        
        if let httpResponse = response as? HTTPURLResponse {
            
            if httpResponse.statusCode == 200 {
                
                exists =  true
            }else{
                exists  = false
            }
            
        }
        return exists
    }
    
    func getProfile( completion:@escaping (_ response:JSON)->Void ) {
        
        var session = self.getSession()
        let headers = [
            "Authorization":session["id"].stringValue
        ]
        let memberId = session["userId"].stringValue
        let url = self.baseUrl() + "Members/" + memberId
        
        Alamofire.request(url, method:.get, headers: headers)
            .responseJSON { response in
                
                if let res = response.result.value {
                    let json = JSON(res)
                    
                    let error = json["error"]
                    
                    // ..2.. If any errors, handle the error
                    if error != nil {
                        self.httpErrorHandler(err: res as AnyObject)
                        return
                    }
                    
                    completion(json)
                }
        }
    }
    
    func saveProfile( completion:@escaping (_ response:JSON)->Void ) {
        
        
        var session = self.getSession()
        let headers = [
            "Authorization":session["id"].stringValue
        ]
        let memberId = session["userId"].stringValue
        let url = self.baseUrl() + "Members/" + memberId
        
        var params = [String:Any]()
        
        if let firstname = self.firstName.text {
            params["firstname"] =  firstname
        }
        
        if let lastname = self.lastName.text {
            params["lastname"] =  lastname
        }
        
        if let email = self.email.text {
            params["email"] = email
        }
        
        
        if let company = self.companyName.text {
            params["company"] = company
        }
        
        if let bio = self.bio.text {
            params["bio"] = bio
        }
        
        Alamofire.request(url, method:.put, parameters:params, headers: headers)
            .responseJSON { response in
                
                if let res = response.result.value {
                    let json = JSON(res)
                    
                    let error = json["error"]
                    
                    // ..2.. If any errors, handle the error
                    if error != nil {
                        self.httpErrorHandler(err: res as AnyObject)
                        return
                    }
                    
                    completion(json)
                }
        }
    }
    
    func uploadPreview(endPoint:String, image:URL, uploadPhoto:Bool, success:@escaping ()->Void ) {
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if uploadPhoto == true {
                    multipartFormData.append(image, withName: "profileThumbnail")
                }
        },
            to:endPoint,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        success()
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        }
            
        )
    }
    
}
