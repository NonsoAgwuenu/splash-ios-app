//
//  PlayerOverlay.swift
//  splash
//
//  Created by Christopher Kendrick on 8/23/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import Foundation
import UIKit

protocol PlayerOverDelegate {
    func onAddSplash()
    func onTapExit()
    func onAcceptPost()
    func onDenyPost()
    func onEdit()
    func onDelete()
}

class PlayerOverlay:UIView {
    @IBOutlet weak var bottomLabel: UILabel!
    
    @IBOutlet weak var splashAddedLabel: UILabel!
    @IBOutlet weak var acceptBtns: UIStackView!
    @IBOutlet weak var subLabel: UILabel!
    var delegate:PlayerOverDelegate? = nil
    @IBOutlet weak var loadView: UIView!
    @IBOutlet weak var loadViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var addSplashBtn: UIButton!
    @IBOutlet weak var timestamp: UILabel!
    
    @IBOutlet weak var splashBtnImg: UIImageView!
    @IBOutlet weak var thumbnail: UIImageView!
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var denyBtn: UIButton!
    @IBOutlet weak var textInputBox: UITextField!

    @IBOutlet weak var editBtn: UIButton!
    var isEditingInput:Bool = false
    
    
    @IBOutlet weak var delBtn: UIButton!
    
    @IBAction func exit(_ sender: UIButton) {
        self.delegate?.onTapExit()
    }
    
    @IBAction func AddSplash(_ sender: UIButton) {
        self.delegate?.onAddSplash()
    }

    @IBAction func acceptPost(_ sender: UIButton) {
        self.delegate?.onAcceptPost()
    }
    @IBAction func denyPost(_ sender: UIButton) {
        self.delegate?.onDenyPost()
    }
    @IBAction func editPost(_ sender: UIButton) {
        self.delegate?.onEdit()
    }
    @IBAction func deletePost(_ sender: UIButton) {
        self.delegate?.onDelete()
    }
    
    
}
