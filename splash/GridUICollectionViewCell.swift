//
//  GridUICollectionViewCell.swift
//  splash
//
//  Created by Christopher Kendrick on 10/22/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit

class GridUICollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var splashCount: UILabel!
    @IBOutlet weak var title: UITextView!
    @IBOutlet weak var thumbnail: UIImageView!
}
