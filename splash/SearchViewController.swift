//
//  SearchViewController.swift
//  splash
//
//  Created by Christopher Kendrick on 7/21/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Haneke

class SearchUser{
    let username:String!
    let id:String!
    
    init(username:String, id:String){
        self.username = username
        self.id = id
    }
}

class SearchViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    

    @IBOutlet weak var searchBar:UISearchBar!
    @IBOutlet weak var searchResultsTable: UITableView!
    
    var searchActive : Bool = false
    var filtered:[String] = []
    var data = [Member]()
    var selectedMember:Member? = nil
    
    override func viewWillAppear(_ animated: Bool) {
        self.hideKeyboardWhenTappedAround()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        self.searchResultsTable.delegate = self
        self.searchResultsTable.dataSource = self
        // Do any additional setup after loading the view.

    }
    
    override func dismissKeyboard() {
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchUser(searchText)
    }
    
    func searchUser(_ username:String){
        var session = self.getSession()
        
        let params = [
            "filter":[
                "where":[
                    "username":username
                ]
            ]
        ]
        let headers = [
            "Authorization":session["access_token"].stringValue
        ]
        Alamofire.request(self.baseUrl() + "Members/findOne", method:.get, parameters: params, headers: headers)
            .responseJSON { response in
                if let res = response.result.value {
                    let json = JSON(res)
                    let status = json["error"]["status"]
                    if status != 404 {
                        let username = json["username"].stringValue
                        let fname = json["lastname"].stringValue
                        let lname = json["firstname"].stringValue
                        let bio = json["bio"].stringValue
                        let id = json["id"].stringValue
                        
                        let member:Member = Member(id: id, lastname: lname, firstname: fname, bio: bio, username: username)
                        
                        self.data.append(member)
                        self.searchResultsTable.reloadData()
                    }else{
                        self.data = []
                        self.searchResultsTable.reloadData()
                    }
                    
                }
        }
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("CLOSE SEARCH BAR")
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedMember = self.data[indexPath.row]
        self.performSegue(withIdentifier: "topp", sender: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "memberCell") as! MemberTableViewCell

        cell.memberUsername.text = data[(indexPath as NSIndexPath).row].username
        cell.btnAction.tag = indexPath.row
        
        cell.profilePic.hnk_setImage(from: URL(string: self.baseUrl(isApi:false) + "videos/" + data[(indexPath as NSIndexPath).row].id + "/profile.jpg" ))
        
        return cell;
    }

    @IBAction func addFriend(_ sender: UIButton) {
        print(self.data.first?.id)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "topp" {
            let profileVC:ProfileViewController = segue.destination as! ProfileViewController
            profileVC.member = self.selectedMember
            
        }

        
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }


}
