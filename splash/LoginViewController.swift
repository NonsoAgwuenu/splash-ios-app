//
//  LoginViewController.swift
//  splash
//
//  Created by Christopher Kendrick on 7/9/16.
//  Copyright © 2016 Christopher Kendrick. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func login() {
        let email = self.email.text! as String
        let password = self.password.text! as String
        
        let params = ["email":email,"password":password]
        
        // ..1.. Make request to login Api
        Alamofire.request( self.baseUrl() + "Members/login", method:.post, parameters: params)
            .responseJSON { response in
                if let res = response.result.value {
                    let json = JSON(res)
                    let error = json["error"]
                    
                    // ..2.. If any errors, handle the error
                    if error != nil {
                        self.httpErrorHandler(err: res as AnyObject)
                        return
                    }
                    
                    // ..3.. Save the session
                    self.saveSession(json)
                    self.getMember(){ member in
                        self.saveMember(member: member)
                    }
                    self.saveDeviceToken(target: self, completion: { member in 
                        print(member)
                    })
                    // ..4.. Go to main tabs
                    self.goToPage("Main", viewControllerName: "mainTabs")
                    
                    
                }
        }
    }
    
    @IBAction func loginUser(_ sender: UIButton) {
        self.login()
    }
    
    func getMember(cb:@escaping (_ res:JSON)->Void){
        // ..2.. MemberId
        var session = self.getSession()
        var memberId = session["userId"].stringValue
        
        let headers = [
            "Authorization":session["id"].stringValue
        ]
        
        // ..1.. Make request to login Api
        Alamofire.request( self.baseUrl() + "Members/" + memberId, method:.get, headers:headers)
            .responseJSON { response in
                if let res = response.result.value {
                    let json = JSON(res)
                    let error = json["error"]
                    
                    // ..2.. If any errors, handle the error
                    if error != nil {
                        self.httpErrorHandler(err: res as AnyObject)
                        return
                    }
                    
                    cb(json)
                    
                    
                }
        }
    }
    
    func getDeviceToken()->String {
        let sessionStore = UserDefaults.standard
        var deviceToken = ""
        
        if sessionStore.object(forKey: "deviceToken") != nil {
            deviceToken = sessionStore.object(forKey: "deviceToken") as! String
        }
        
        return deviceToken
    }
    
    
    private func saveDeviceToken(target:UIViewController, completion:@escaping ( _ member:JSON )->Void ){
        var session = self.getSession()
        var memberId = session["userId"].stringValue
        
        let headers = [
            "Authorization":session["id"].stringValue
        ]
        
        let token = self.getDeviceToken()
        let params = ["deviceToken":token]
        
        
        Alamofire.request(self.baseUrl() + "Members/" + memberId, method:.put, parameters: params, headers:headers)
            .responseJSON { response in
                if let res = response.result.value {
                    let json = JSON(res)
                    let error = json["error"]
                    
                    // ..2.. If any errors, handle the error
                    if error != nil {
                        target.httpErrorHandler(err: res as AnyObject)
                        return
                    }
                    
                    
                    completion(json)
                }
        }
    }


}
