//
//  FriendTableViewCell.swift
//  splash
//
//  Created by Christopher Kendrick on 1/12/17.
//  Copyright © 2017 Christopher Kendrick. All rights reserved.
//

import UIKit

class FriendTableViewCell: UITableViewCell {

    
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var btn: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.thumbnail.layer.cornerRadius = self.thumbnail.bounds.width/2
        self.thumbnail.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
