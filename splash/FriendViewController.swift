//
//  FriendViewController.swift
//  splash
//
//  Created by Christopher Kendrick on 1/10/17.
//  Copyright © 2017 Christopher Kendrick. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Friend {
    
    var id:String?
    var memberId:String?
    var workerId:String?
    var follower:Member? = nil
    var foll:Member? = nil
    init(id:String? = "", memberId:String? = "", workerId:String? = "", follower:Member? = nil, foll:Member? = nil) {
        self.id = id
        self.memberId = memberId
        self.workerId = workerId
        self.follower = follower
        self.foll = foll
    }
    
    func instanceFromJSON(friend:JSON)->Friend{
        
        let id = friend["id"].stringValue
        let memberId = friend["memberId"].stringValue
        let workerId = friend["workerId"].stringValue
        let followerJson = friend["member"]
        
        let foll = friend["friend"]
        let follMember:Member = Member().instanceFromJSON(member: foll)
        
        let follower:Member = Member().instanceFromJSON(member: followerJson)
        return Friend(id: id, memberId: memberId, workerId: workerId, follower:follower, foll:follMember)
    }
    
    func arrInstanceFromJSONArr(members:[JSON])->[Friend]{
        return members.map({ self.instanceFromJSON(friend: $0) })
    }
    
}

class FriendViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var friendTable: UITableView!
    var member:Member? = nil
    var followers = [Friend]()
    var selectedFriend:Member? = nil
    var whoUserFollowingType:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.friendTable.delegate = self
        self.friendTable.dataSource = self
        // Do any additional setup after loading the view.
        
        self.friends(){ friends in
            let f:Friend = Friend()
            self.followers.append(contentsOf: f.arrInstanceFromJSONArr(members: friends.arrayValue))
            self.friendTable.reloadData()
        }
        
    }

    @IBAction func action(_ sender: UIButton) {
        let friend = self.followers[sender.tag]
        let userId = self.getSession()["userId"].stringValue
        self.removeFriend(memberId: userId, friendId: friend.id!){ count in
            print(count)
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(self.whoUserFollowingType){
            self.selectedFriend = self.followers[indexPath.row].foll
        }else{
            self.selectedFriend = self.followers[indexPath.row].follower
        }
        
        self.performSegue(withIdentifier: "showProfileFollowing", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func exit(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: {})
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.followers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "friendCell") as! FriendTableViewCell
        
        let friend = self.followers[indexPath.row]
        
        if(self.whoUserFollowingType) {
            cell.label.text = friend.foll?.username
            cell.thumbnail.hnk_setImage(from: URL(string: self.baseUrl(isApi:false) + "videos/" + (friend.foll?.id)! + "/profile.jpg" ))
        }else{
            cell.label.text = friend.follower!.username
            cell.thumbnail.hnk_setImage(from: URL(string: self.baseUrl(isApi:false) + "videos/" + (friend.follower?.id)! + "/profile.jpg" ))
        }

        
        return cell
    }
    
    func removeFriend(memberId:String, friendId:String, completion:@escaping (_ count:JSON)->Void ){
        
        let accessToken = self.getSession()["id"].stringValue
        let headers = [
            "Authorization":accessToken
        ]
        
        Alamofire.request(self.baseUrl() + "/Members/" + memberId + "/incomingFriendRequest/" + friendId , method:.delete, headers:headers)
            .response(
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    
                    if let res = response.result.value {
                        let json = JSON(res)
                        let error = json["error"]
                        
                        // ..2.. If any errors, handle the error
                        if error != nil {
                            _ = self.httpErrorHandler(err: res as AnyObject)
                            return
                        }
                        
                        completion(json)
                        
                        
                    }
                    
        })
    }
    
    func friends( _ completion:@escaping (_ res:JSON)->Void ){
        
        let accessToken = self.getSession()["id"].stringValue
        let headers = [
            "Authorization":accessToken
        ]
        
        let params = [
            "filter":[
                "include":["member","friend"]
            ]
        ]
        
        var memberId = self.member?.id
        
        if self.member == nil {
            memberId = self.getSession()["userId"].stringValue
        }
        
        var method:String = "/incomingFriendRequest"
        
        if(self.whoUserFollowingType){
            method = "/friends"
        }
        
        Alamofire.request(self.baseUrl() + "/Members/" + memberId! + method, method:.get, parameters:params, headers:headers)
            .response(
                responseSerializer: DataRequest.jsonResponseSerializer(),
                completionHandler: { response in
                    print(response)
                    // Validate your JSON response and convert into model objects if necessary
                    
                    if let res = response.result.value {
                        let json = JSON(res)
                        let error = json["error"]
                        
                        // ..2.. If any errors, handle the error
                        if error != nil {
                            _ = self.httpErrorHandler(err: res as AnyObject)
                            return
                        }
                        
                        completion(json)
                        
                    }
                    
            }
        )}
    
    


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let profileViewControler:ProfileViewController = segue.destination as! ProfileViewController
        profileViewControler.member = self.selectedFriend
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }


}
