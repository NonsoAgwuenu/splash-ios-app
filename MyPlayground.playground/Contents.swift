//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


func getPricesFromJSON(price:[[String:Any]])->[ProductPrice]{
    
    print("price from array")
    print(price)
    
    var pricesArr = [ProductPrice]()
    
    for p in price{
        
        var pro:ProductPrice = ProductPrice()
        
        
        let labels = JSON(p)["label"].arrayObject as! [String]
        let prices = JSON(p)["price"].arrayObject as! [String]
        
        
        for label in labels {
            pro.label = label
        }
        for price in prices {
            pro.price = NSDecimalNumber(string: price)
        }
        
        pricesArr.append(pro)
        
    }
    
    print(pricesArr)
    
    return pricesArr
}